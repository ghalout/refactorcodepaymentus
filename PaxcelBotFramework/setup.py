from setuptools import setup,find_packages
import sys
import os

buildFilePath=os.path.join(os.getcwd(),"buildNumber.txt")
with open(buildFilePath) as f:
    oldVersion=f.readlines()[0]

splitted=oldVersion.split(".")
head=".".join(splitted[0:-1])
tail=int(splitted[-1])

if sys.argv[-1] =="--incVersion":
    sys.argv.pop(-1)
    newVersion=head + str(".")+str(tail+1)
else:
    newVersion = head + str(".") + str(tail)

with open(buildFilePath,"w") as f:
    f.write(newVersion)


setup(
    name='PaxcelBotFramework',
    version=newVersion,
    license='MIT',
    packages=find_packages()
)
