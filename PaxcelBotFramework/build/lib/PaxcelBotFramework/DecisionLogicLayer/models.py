from abc import abstractmethod,ABCMeta              # for abstract classes
from intellect.Intellect import Intellect
import uuid

# Create your views here.

class DecisionLogicSolver():
    """

        This is the abstract base class method
        from which all Decision Logic solvers will be derived

    """
    __metaclass__ = ABCMeta

    def __init__(self, policyFilePath, name):

        """

        :param policyFilePath: Policy file path for the bot
        :param name: A User assigned string which names the decision logic solver

        """

        self.policyFilePath = policyFilePath
        self.name = name

    @abstractmethod
    def getResponse(self, policyInputObject):
        """

        :param policyInputObject: the object type as accepted by the policy file


        """
        pass


class IntellectObject(Intellect):
    pass


class IntellectLogicSolver(DecisionLogicSolver):
    """

        This is the Intellect Decision solver


    """

    def __init__(self, policyFilePath, name="IntellectLogicSolver_"+str(uuid.uuid4())):

        """

        :param policyFilePath: Policy file path for the bot
        :param name: A User assigned string which names the decision logic solver

        """
        super(IntellectLogicSolver, self).__init__(policyFilePath, name)



    def getResponse(self, policyInputObject):
        """

        :param policyInputObject: the object type as accepted by the policy file

        should have getter and setter methods which will be modified and operated on upon by the policy file


        @property
        def property0(self):
            return self._property0

        @property0.setter
        def property0(self, value):
            self._property0 = value

        :return: modified policyInputObject



        """

        intellectObject=IntellectObject()

        # provide the policy file path
        intellectObject.learn(Intellect.local_file_uri(self.policyFilePath))

        # provide the intent
        intellectObject.learn(policyInputObject)

        intellectObject.reason()
        return intellectObject.knowledge[0]


class SolverFactory():
    """

    Factory for returning the requested Solver type

    """
    def __init__(self):
        pass

    def getSolverInstance(self,type,policyFilePath,name=''):
        if type=="Intellect": ## load these from configs
            if name:
                return IntellectLogicSolver(policyFilePath,name)
            else:
                return IntellectLogicSolver(policyFilePath)



