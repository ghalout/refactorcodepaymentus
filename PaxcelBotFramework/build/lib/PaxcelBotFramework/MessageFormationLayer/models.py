from abc import  ABCMeta,abstractmethod
from PaxcelBotFramework.BotManager.models.botResponse import BotResponse

# Create your views here.


class Data():

    """

    This is the base class from which every data type inherits

    """

    __metaclass__ = ABCMeta


    def __init__(self,content,message,tag):

        self.content=content
        self.message=message
        self.tag=tag


    @abstractmethod
    def processTag(self):
        """

        In case we need to do some pre processing

        """

        pass



class SimpleMessage(Data):

    def __init__(self,content="",message="",tag=""):
        """

        :param content: string type message
        :param tag: the tag function can be used for defining the processTag operation and modify the content

        """
        super(SimpleMessage, self).__init__(content=content,message=message,tag=tag)



    def processTag(self):

        """

        In case we need to do some pre processing according to tag this class can be inherited and this
        function can be modified

        """

        pass




class MessageLayerModel():

    def __init__(self,content=[SimpleMessage()],botResponse=BotResponse()):
        self.content =content
        self.botResponse=botResponse





