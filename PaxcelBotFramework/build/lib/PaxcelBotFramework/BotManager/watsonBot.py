from watson_developer_cloud import ConversationV1   # Support for accessing watson conversation api
from models.botResponse import BotResponse
from models.botRequest import BotRequest
from models.intents import Intent
from models.entities import Entity
from models.messages import Message
from models.relations import NullRelation,Relation,AndOrBetweenEntityRelation
from models.dialogNodes import DialogueNode
from models.context import Context,ContextElement
import uuid
from botTemplate import Ibot



class WatsonBot(Ibot):
    """
    The Watson Bot Class which defines how to access
    the watson api and get its response, inherits from the "Bot" class

    """
    def __init__(self,authenticationParameters,name="WatsonBot_"+str(uuid.uuid4())):
        """

        :param authenticationParameters:

                the authentication parameters needed to access the bot

                {
                    'username' : '212e8b97-d3c7-4edf-a759-1150b32e3d87',
                    'password' : 'MzlBgFHfA1lS',
                    'version' :  '2017-06-12',
                    'workSpaceId' :'c1c8ed6a-5955-4b12-8a01-b5bd6813495c'

                }

        :param name: The name of the bot , defaults to WatsonBot_uniqueIdForBot

        """

        # call to the init method of the base class
        super(WatsonBot,self).__init__(authenticationParameters,name)
        self.username       = authenticationParameters.username,
        self.password       = authenticationParameters.password,
        self.version        = authenticationParameters.version
        self.workSpaceId    = authenticationParameters.workSpaceId

    def getResponse(self,botRequest):

        """

        :param botRequest: a "BotRequest" object which is the users request containing the message and
        :return: "BotResponse" object which constitutes the bots response

        """


        ##  extract messageBody and messageContext from the botRequest
        messageBody, messageContext,computeRelations,origContext = WatsonBot.parseBotRequestObject(botRequest)


        conversationObject = ConversationV1(
            username=self.username[0],
            password=self.password[0],
            version=self.version
        )



        watsonResponse=conversationObject.message(workspace_id=self.workSpaceId, message_input=messageBody,
                                  alternate_intents=True, context=messageContext)


        ## Format the watsonResponse to BotResponse
        return WatsonBot.formatBotResponse(watsonResponse,computeRelations,origContext)


    ############################### Input Output Formatting ##################################
    ##### Input Formating #####

    @staticmethod
    def parseBotRequestObject(botRequest):

        ## convert the botRequest object  to format accepted by watson
        message=WatsonBot.parseInputMessage(botRequest.message)

        ## parse the bot context here
        watsonContext=WatsonBot.parseInputContext(botRequest.context)

        ## orignal Context
        origContext = botRequest.context

        ## the relations that have to be computed at this stage
        computeRelations=botRequest.computeRelations

        messageBody = {
            "text": message.replace("\n", ""), # required because of watson format
        }

        return messageBody,watsonContext,computeRelations,origContext

    @staticmethod
    def parseInputContext(context):

        if len(context.contextElements)>=1:
            lastContextElem=context.contextElements[-1]
            cont=lastContextElem.raw
        else:
            cont={}


        return cont

    @staticmethod
    def parseInputMessage(message):
        return message.message


    ##### Output Formatting #####
    @staticmethod
    def formatBotResponse(response,computeRelations,origContext):

        ### Convert the watsonResponse to the BotResponse Format ###

        ### add the default relations to be computed everytime here ###

        intents     =   WatsonBot.extractIntents(response)
        entities    =   WatsonBot.extractEntities(response)

        # print intents,entities
        inputMessage,outputMessages     =      WatsonBot.extractInputOutputMessage(response)
        if entities:
            inputMessage.substituteEntities(entities)

        context=WatsonBot.extractContext(response,origContext)

        botResponse=BotResponse(intents=intents,entities=entities,relations=[],inputMessage=inputMessage,outputMessages=outputMessages,context=context)

        return botResponse


    #### response parsing helper methods ###
    @staticmethod
    def computeRelations(computeRelations,entities=[]):

        relations=[]
        for relation in computeRelations:
            if relation.computeRelation:
                pass

        pass

    @staticmethod
    def extractEntities(response):
        """
        Convert
         {
              "confidence": 1,
              "location": [
                9,
                16
              ],
              "value": "Vaibhav Arora",
              "entity": "Employee"
        }
        object to Entity Object

        returns list of entity objects captured

        """

        if not response:
            raise Exception("Bot not run !")
        else:

            # Capture entities from response

            if response['entities']:

                message=response["input"]["text"]

                entities = [ Entity(
                                    name=entDict["value"],
                                    type=entDict["entity"],
                                    location=entDict["location"],
                                    confidence=entDict["confidence"],
                                    literal=message[entDict["location"][0]:entDict["location"][1]]
                                    )

                             for entDict in response["entities"]

                ]

                return entities


    @staticmethod
    def extractIntents(response):

        """

        returns list of intent objects captured


        """

        if not response:
            raise Exception("Bot not run !")
        else:
            # Capture intents from response
            intents = []
            if response['intents']:
                ## Create a list of intent objects
                intents=[Intent(intDict['intent'], intDict['confidence']) for intDict in response['intents']]
            return intents



    @staticmethod
    def extractInputOutputMessage(response):

        """
        :param response: The watson response (json format)

        :return:

        inputMessage : A message object of input type ,
        outputMessages : A list of output type Message Types

        """

        inputMessage   = Message(response["input"]['text'],type='input')
        outputMessages = [Message(mes,type='output',raw=response["output"]) for mes in response["output"]['text']]

        return inputMessage,outputMessages

    @staticmethod
    def extractContext(response,context):
        """

        :param response: the returned watson response variable
        :return: A Context object

        """
        conversationId = response["context"]["conversation_id"]
        dialogCounter = response["context"]["system"]["dialog_turn_counter"]
        nodesVisited = []
        #try :
        print "#"*1000
        print dialogCounter


        try:
            nodesVisited = [DialogueNode(name="", id=id, description="") for id in response["output"]["nodes_visited"]]
        except:
            pass

        entities = WatsonBot.extractEntities(response)
        intents=WatsonBot.extractIntents(response)

        """
        try:
            entities = [Entity(
                name=response["context"][key],
                type=key,
                location="",
                confidence=0.0,
                literal=""
            )
                for key in response["context"].keys() if key not in ['conversation_id', 'system']]
        except:
            pass
        """



        contextElem=ContextElement(
            intents=intents,
            conversationId=conversationId,
            entities=entities,
            nodesVisited=nodesVisited,
            relations=[],
            dialogCounter=dialogCounter,
            raw=response['context']

        )

        context.addContextElement(contextElement=contextElem)

        return context



