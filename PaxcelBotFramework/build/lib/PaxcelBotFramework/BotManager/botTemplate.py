# -*- coding: utf-8 -*-
from __future__ import unicode_literals             # line no 1 and this line is for avoiding unicode errors

from abc import abstractmethod,ABCMeta              # for abstract classes


class Ibot():
    """

    This is the abstract base class method
    from which all bots will be derived

    """
    __metaclass__ = ABCMeta

    def __init__(self,authenticationParameters,name):
        """

        :param authenticationParameters: Authentication Parameters for any bot
        :param name: A User assigned string which names the bot

        """

        self.authenticationParameters = authenticationParameters
        self.name=name



    @abstractmethod
    def getResponse(self,botRequest):
        """

        :param botRequest: "BotRequest" object
        :return: "BotResponse" object

        """
        pass









