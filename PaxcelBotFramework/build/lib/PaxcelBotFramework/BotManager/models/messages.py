import re

class Message():

    """

    A Model for bot Input and Output Message

    """

    def __init__(self, message="", type="input", raw={}):

        """

        :param message: User Message string , eg Who Works in Drutas
        :param type: Indicating the message type i.e. "input" or "output"
        :param raw: Not defined for the input message type, contains the raw json output returned from the bot api



        """

        self.message = message

        ## Indicating the Message Type
        self.type = type

        ##
        ## Substituting all synonyms for their name eg in this case Who works in Agile Project Management Suite
        ## This can be calculated by using the substituteEntities Method - will not work for output messages
        ##

        self.entitesSubstituted = ""

        ##
        ## substituting the raw returned json only valid for non input values
        ##

        if self.type == 'input':
            self.raw = {}
        else:
            self.raw = raw

    def substituteEntities(self, entities):

        """

        ##
        ## Substitute  entity literals in message with entity names
        ##
        ## Conflicts may arise in case two entities overlap, currently the longest entity will be substituted first
        ##
        ## Note : Write Better function to resolve this conflict, also test this thoroughly
        ##


        :param entities: A list of entity objects


        """

        if self.type == "output":
            raise (
            Warning("Output Message Type does'nt support this ! messageEntitesSubstituted wont be changed !!"))
        else:

            ## Sorts the entities in descending order of Literal Length

            entities.sort(reverse=True, key=lambda x: x.literalLength)
            messageVar = self.message
            for entity in entities:
                try:
                    ## replace entities

                    messageVar = re.sub('\b' + entity.literal + '\b', '\b' + entity.name + '\b', messageVar)

                except:
                    pass

            self.entitesSubstituted = messageVar
