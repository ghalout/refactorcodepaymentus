class DialogueNode():
    def __init__(self,name="",id="",description=""):
        """

        :param name: Name of the dialog node
        :param id: Id of the dialog node
        :param description: The description of the dialog node
        """

        self.name=name
        self.id=id
        self.descritpion=description

    def toJson(self):
        return {"name":self.name,"id":self.id,"description":self.descritpion}
