class Intent():

    def __init__(self,name="",confidence=0.0):
        """
        :param name: Name of the intent
        :param confidence: Degree of confidence of the bot in this intent
        """
        self.name=name
        self.confidence=float(confidence)

    def toJson(self):
        return {"name":self.name,"confidence":self.confidence}