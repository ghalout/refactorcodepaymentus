# -*- coding: utf-8 -*-
from __future__ import unicode_literals             # line no 1 and this line is for avoiding unicode errors

from messages import Message
from relations import NullRelation
from context import Context


class BotRequest():

    """

    The user sends this model to the bot, Any bot derived from the "Bot" class will accept request in this format
    only, it contains the user message and context


    """
    def __init__(self,message=Message(""),context=Context(),computeRelations=[NullRelation()]):

        """

        :param message: A message Object from the user
        :param context: Chat context

        """

        self.message=message
        self.context=context
        # raw message , entities compute
        # and or relation , entities specify, entities message compute, ignore
        self.computeRelations=computeRelations

