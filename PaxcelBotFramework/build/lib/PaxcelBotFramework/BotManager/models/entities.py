class Entity():
    def __init__(self,name="",type="",confidence=0.0,location="",literal=""):
        """

        :param name: Exact name of the entity eg Agile Project Management Suite
        :param type: Type eg Project
        :param confidence: Degree of confidence in the entity
        :param location: [start,end] a list indicating starting and the ending position in the message string
        :param literal: exact value the user wrote eg Drutas

        """
        self.name=name
        self.type=type
        self.confidence=float(confidence)
        self.location=location
        self.literal=literal
        self.literalLength=len(literal)

    def toJson(self):
        """

        :return: attributes as json

        """
        jsonified={
            "name":self.name,
            "type":self.type,
            "confidence":self.confidence,
            "location":self.location,
            "literal":self.literal,
            "literalLength":self.literalLength
        }
        return jsonified