from intents import Intent
from entities import Entity
from relations import NullRelation
from messages import Message
from context import  Context


class BotResponse():

    """

    Any bot derived from the "Bot" Class will return the response in this format only
    Contains intents , entities,relations,message, context

    """

    def __init__(self,intents=[Intent()],entities=[Entity()],relations=[NullRelation()],inputMessage=Message(),outputMessages=[Message()],context=Context()):

        """

        :param intents:   List of intent objects
        :param entities:  List of entity objects
        :param relations: List of Relation Objects
        :param inputMessage:  Message Object of Input Type
        :param outputMessage:  List of Message Objects of Output Type
        :param context:   Context Object


        """
        self.intents=intents
        self.entities=entities
        self.relations=relations
        self.inputMessage=inputMessage
        self.outputMessages = outputMessages
        self.context=context









