# -*- coding: utf-8 -*-
from __future__ import unicode_literals             # line no 1 and this line is for avoiding unicode errors

### Importing different Models ###
import logging
from intents import Intent
from entities import Entity
from dialogNodes import DialogueNode
from relations import NullRelation
from messages import Message

logger = logging.getLogger(__name__)

class ContextElement():


    ### A context element denotes a single message exchange


    def __init__(self,conversationId="",intents=[],entities=[],nodesVisited=[],relations=[],raw={},dialogCounter=0):

        self.conversationId=conversationId
        self.entities=entities
        self.nodesVisited=nodesVisited
        self.relations=relations
        self.raw=raw
        self.dialogCounter=dialogCounter
        self.intents=intents

    def toJson(self):
        contextElement={}
        contextElement["conversationId"]=self.conversationId
        contextElement["intents"]=[intent.toJson() for intent in self.intents]
        contextElement["entities"]=[entity.toJson() for entity in self.entities]
        contextElement["nodesVisited"]=[node.toJson() for node in self.nodesVisited]
        contextElement["relations"]=[relation.toJson() for relation in self.relations]
        contextElement["raw"]=self.raw
        contextElement["dialogCounter"]=self.dialogCounter
        return contextElement



class Context():

    ## A Model for bot Context, contextElements contains a list of ContextElements

    def __init__(self,conversationId="",contextElements=[],topIntents={},entities={}):

        self.conversationId=conversationId
        self.contextElements=contextElements
        self.topIntents=topIntents
        self.entities=entities

    def toJson(self):
        contextJson={}
        contextJson["conversationId"]=self.conversationId
        contextJson["contextElements"]=[contextElement.toJson() for contextElement in self.contextElements]
        contextJson["topIntents"]=self.topIntents
        contextJson["entities"]=self.entities
        return contextJson


    def findIntentOccurrence(self,intentName):
        intentOccurences=[]

        if intentName in self.topIntents.keys():
            intentOccurences=self.topIntents[intentName]["dialogCounters"]
        return intentOccurences


    def findEntityOccurrence(self,filter={}):

        """
        Note : add supports to include greater than or less than fiterring

        :param filter:
        :return: a list of [dialogCount,entity] elements
        """

        filterAsSet = set([(key, filter[key]) for key in filter.keys()])
        matched=[]
        for dialogCount in self.entities.keys():
            for ent in self.entities[dialogCount]:
                entAsSet = set([(key, str(ent[key])) for key in ent.keys()])
                if filterAsSet.issubset(entAsSet):
                    matched.append([dialogCount,ent])
        return matched

    def findIntentCount(self,intentName):
        """

        Find out how many times the intent was the top intent in the conversation

        :param intentName:
        :return:

        """

        count=0

        if intentName in self.topIntents.keys():
            count=len(self.topIntents[intentName]["dialogCounters"])


        return count


    def findEntityCount(self,filter={}):

        """

        :param filter: specify the filters by which you want to match the entities
        :return:
        """

        count =len(self.findEntityOccurrence(filter=filter))

        return count




    def addContextElement(self,contextElement=ContextElement()):
        if contextElement.dialogCounter >0:

            self.conversationId=contextElement.conversationId

            if self.conversationId != contextElement.conversationId:
                logger.warn("Old conversation Id does not match new conversation id ! {0} != {1} ".format(self.conversationId,contextElement.conversationId), extra={"oldConversationId" : self.conversationId,"newConversationId":contextElement.conversationId})


            self.contextElements.append(contextElement)

            """
            
            code for updating the context element
            
            """
            if contextElement.intents:
                topIntName=contextElement.intents[0].name
                topIntConfidence=contextElement.intents[0].confidence
                topIntDialogCounter=contextElement.dialogCounter

                if topIntName:
                    # to avoid blank intents
                    if  topIntName in self.topIntents.keys():

                        # intent has already occurred in the dialog

                        self.topIntents[topIntName]["dialogCounters"].append(topIntDialogCounter)
                        self.topIntents[topIntName]["confidence"].append(topIntConfidence)
                        self.topIntents[topIntName]["numOccurrence"] += 1
                        self.topIntents[topIntName]["lastLocDiff"]    = self.topIntents[topIntName]["dialogCounters"][-1]-self.topIntents[topIntName]["dialogCounters"][-2]

                    else:

                        # Intent encountered for the first time
                        self.topIntents[topIntName]={}

                        self.topIntents[topIntName]["dialogCounters"]=[topIntDialogCounter]
                        self.topIntents[topIntName]["confidence"]=[topIntConfidence]
                        self.topIntents[topIntName]["numOccurrence"] = 1
                        self.topIntents[topIntName]["lastLocDiff"] = 0
                        print self.topIntents

            if contextElement.entities:

                # Updating entities
                try:

                    self.entities[contextElement.dialogCounter] =[ent.toJson() for ent in contextElement.entities]
                except:
                    print "***********"
                    print contextElement.dialogCounter
                    print contextElement.toJson()



















