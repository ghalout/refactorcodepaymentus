from entities import Entity
from intents import Intent
from dialogNodes import DialogueNode
from relations import Relation,NullRelation
from context import Context,ContextElement

intent1=Intent(confidence=0.7,name="greeting")
intent2=Intent(confidence=0.8,name="orderPizza")

entity1=Entity(name="veg",type="pizzaType",confidence=1.0,location=[4,14],literal="vegitarian")

entity2=Entity(name="non veg",type="pizzaType",confidence=1.0,location=[15,29],literal="non vegitarian")


contextElem1=ContextElement(conversationId="15",intents=[intent1],entities=[entity1],nodesVisited=[DialogueNode()],
                       relations=[NullRelation()],raw={},dialogueCounter=1)

contextElem2=ContextElement(conversationId="15",intents=[intent2],entities=[entity2],nodesVisited=[DialogueNode()],
                       relations=[NullRelation()],raw={},dialogueCounter=2)

cont=Context(conversationId="15",contextElements=[],topIntents={},entities={})

cont.addContextElement(contextElem1)
cont.addContextElement(contextElem2)

print cont.findIntentCount("greeting")
print cont.findEntityCount({"type":"pizzaType"})
print cont.findIntentOccurrence("greeting")
print cont.findIntentOccurrence("greeting")
print cont.findEntityOccurrence({"type":"pizzaType"})



# A Model for bot Context, contextElements contains a list of ContextElements


#def __init__(self, conversationId="", contextElements=[ContextElement()], topIntents={}, entities={}):



