from watsonBot import WatsonBot
#############################################################################################
##################################### Factories #############################################
#############################################################################################


class BotFactory():
    """
    Factory for returning the requested bot type

    """
    def __init__(self):
        pass

    def getBotInstance(self,type,authenticationParameters,name):
        if type=="WatsonBot": ## load these from configs
            if name:
                return WatsonBot(authenticationParameters,name)
            else:
                return WatsonBot(authenticationParameters)
