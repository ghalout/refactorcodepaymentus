from ipware.ip import get_ip
from abc import ABCMeta,abstractmethod

from PaxcelBotFramework.BotManager.models.intents import Intent
from PaxcelBotFramework.BotManager.models.entities import Entity
from PaxcelBotFramework.BotManager.models.messages import Message
from PaxcelBotFramework.BotManager.models.dialogNodes import DialogueNode
from PaxcelBotFramework.BotManager.models.context import Context,ContextElement
from PaxcelBotFramework.BotManager.models.botRequest import BotRequest


from PaxcelBotFramework.MessageFormationLayer.models import MessageLayerModel,SimpleMessage
import ast
from yattag import Doc
from messageClasses import tagsDict




class InputProcessor():

    """

    All the input parsers will have this format, this is the abstract base class that specifies that


    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def preprocessRequest(self, request):
        """
        an abstract method to be defined in each of the derived classes

        :param request: request object
        :return:
        """
        pass


class OutputProcessor():

    """

    All the output parsers will have this format, this is the abstract base class that specifies that


    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def processRequest(self):
        """
        an abstract method which is to be defined in each of the derived classes

        :return:
        """
        pass



class InputProcessorType1(InputProcessor):
    """

    A class responsible for handling the input request and extracting all the data from it

    """



    def __init__(self):

        self.requestType=""
        self.requestContent={}
        self.ip=""
        self.leadId=""
        self.userId=""
        self.responseType=""



    def preprocessRequest(self,request):
        # Duties :
        # extract the type ,content , ip from the request
        # update  requestType ,requestContent,ip,leadId,userId

        requestType = request.POST["type"]
        requestContent = request.POST["content"]
        leadId = request.POST["leadId"]
        userId = request.POST["userId"]
        responseType=request.POST["responseType"]
        ip = get_ip(request)

        # print "request Content"
        requestContent = requestContent.replace("true", "'true'")
        if ip is None:
            ip = "We Dont Have IP Address For This User"

        self.requestType = requestType
        self.requestContent = ast.literal_eval(requestContent)
        self.ip = ip
        self.leadId = leadId
        self.userId = userId
        self.responseType=responseType



    def parseAccordingTo(self,BotType="WatsonBot"):

        """
        This function converts the data from the input request into a BotRequest Type object.


        :param BotType: The type of the Bot Used in Bot manager
        :return:  BotRequest Object


        """

        message=Message(message=self.requestContent["message"],type="Input")

        context=Context()

        if BotType=="WatsonBot":


            """
            
            Method to Parse Watson Bot type Contexts from the input json structure
            
            
            """
            ## Initialise Context
            context=Context()

            if self.requestContent["context"]:
                conversationId=self.requestContent["context"]["conversationId"]
                topIntents=self.requestContent["context"]["topIntents"]
                entities=self.requestContent["context"]["entities"]
                context=Context(conversationId=conversationId,topIntents=topIntents,entities=entities)

                for contElem in self.requestContent["context"]["contextElements"]:
                    conversationId = contElem["conversationId"],

                    ## Parse Entities
                    entities =[]

                    for ent in contElem["entities"]:
                        entFormatted=Entity(
                            confidence=ent["confidence"],
                            name=ent["name"],
                            #literalLength=ent["literalLength"], this is initialised by default
                            literal=ent["literal"],
                            location=ent["location"],
                            type=ent["type"]


                        )
                        entities.append(entFormatted)

                    ## Parse Intents
                    intents = []
                    for intent in contElem["intents"]:
                        intFormatted=Intent(
                            name=intent["name"],
                            confidence=intent["confidence"]
                        )
                        intents.append(intFormatted)


                    ## Parse Nodes Visited

                    nodesVisited=[]
                    for node in contElem["nodesVisited"]:
                        nodeFormatted=DialogueNode(
                            name=node["name"],
                            id=node["id"],
                            description=node["description"]


                        )
                        nodesVisited.append(nodeFormatted)

                    ## Parse Relations
                    relations = []


                    ## Parse Dialog Counter
                    dialogCounter = contElem["dialogCounter"],

                    ## Parse as Raw
                    raw = contElem['raw']

                    ## Create Context Element
                    contextElement = ContextElement(
                        conversationId=conversationId,
                        entities=entities,
                        nodesVisited=nodesVisited,
                        relations=[],
                        dialogCounter=dialogCounter,
                        raw=raw

                    )

                    ## Add Context element to Context
                    context.addContextElement(contextElement)

        return BotRequest(message=message,context=context)


class OutputProcessorType1(OutputProcessor):

    """
    This Class is responsible for converting the MessageLayerModel object into output response as
    per the bot specifications

    """

    def __init__(self,outputContent=MessageLayerModel(),botDisplayName="",responseType="html",dataTypeFunctionMap=[]):

        """
        :param outputContent: the output that is to be sent to the frontend,  a MessageLayerModel object
        :param botDisplayName: the name that is to be displayed in the bot icon
        :param responseType:
        :param dataTypeFunctionMap:
        """
        self.outputContent  = outputContent
        self.botDisplayName = botDisplayName
        self.responseType   = responseType
        # loading pre-existing function maps
        self.dataTypeFunctionMap = {
            "SimpleMessage": OutputProcessorType1.processSimpleMessage

        }

        for dataType,functionMap in dataTypeFunctionMap:
            self.dataTypeFunctionMap[dataType]=functionMap

        self.outputDict={}



    def processRequest(self):
        """
        This function processes the contents of the MessageLayerModel and creates an output json according
        to each object

        :return: outputDict dictionary which when converted to json is accepted by the frontend


        """

        # outputDict = self.outputDict
        for responseElement in self.outputContent.content:
            # simple code to extract the string name of the class
            responseElementType=str(type(responseElement)).replace("<class ","").replace(">","").replace("'","").split(".")[-1]
            print "responseElementType",responseElementType
            self.dataTypeFunctionMap[responseElementType](self,responseElement)

        self.outputDict["context"]=self.outputContent.botResponse.context.toJson()

        return self.outputDict




    ##
    ## Element Mapper Functions
    ##
    ## To Add custom Mapper Functions
    ##
    ## Any custom mapper function will have responseElement as the input
    ## also so that the custom mapper function is loaded ensure to add it in the dataTypeFunctionMap
    ## dataTypeFunctionMap =[["elementClassType","functionInstance"]]
    ##
    ##


    def processSimpleMessage(self,responseElement):

        if "message" in self.outputDict.keys():
            if "html" in self.responseType :
                self.outputDict["message"].append(
                    OutputProcessorType1.createHtmlSimpleMessage(responseElement.message, self.botDisplayName))
            elif "json" in self.responseType :
                self.outputDict["message"].append(responseElement.message)

        else:
            if  "html" in self.responseType :
                self.outputDict["message"] = [
                    OutputProcessorType1.createHtmlSimpleMessage(responseElement.message, self.botDisplayName)]
            elif "json" in self.responseType :
                self.outputDict["message"] = [responseElement.message]






    @staticmethod
    def  createHtmlSimpleMessage(message,botDisplayName):

        """
        this function creates the html for a simple message object

        :param message: the message to be displayed
        :param botDisplayName: the bot name to be displayed in the div object
        :return: the created html

        """


        doc, tag, text = Doc().tagtext()

        with tag('div', klass=tagsDict.floatleft):
            with tag('div', klass=tagsDict.fontWeightSize):
                doc.stag('img',
                         ('alt', botDisplayName),
                         ('src', tagsDict.botImageUrl),
                         ('height', tagsDict.botImageHeight),
                         ('width', tagsDict.botImageWidth),
                         ('title', botDisplayName)
                         )

                text(botDisplayName)
            with tag('div', klass=tagsDict.chatMessage):
                with tag('p', klass=tagsDict.messageText):
                    text(message)

        return str(doc.getvalue()).replace('"',"'")















