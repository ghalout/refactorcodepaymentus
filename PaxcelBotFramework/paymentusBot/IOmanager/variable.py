botResponse_variable = {
  'intents': [
    {
      'dialogCounter': 1,
      'confidence': 0.9760584354400634,
      'name': 'getAcoountNoInfo'
    },
    {
      'dialogCounter': 1,
      'confidence': 0.25793172121047975,
      'name': 'autoPaymentStepsInfo'
    },
    {
      'dialogCounter': 1,
      'confidence': 0.25599955916404726,
      'name': 'General_Ending'
    },
    {
      'dialogCounter': 1,
      'confidence': 0.24558325111865997,
      'name': 'connectToAgent'
    },
    {
      'dialogCounter': 1,
      'confidence': 0.2299309462308884,
      'name': 'lateFeesChargeInfo'
    },
    {
      'dialogCounter': 1,
      'confidence': 0.2289618790149689,
      'name': 'raiseTicket'
    },
    {
      'dialogCounter': 1,
      'confidence': 0.22560224235057832,
      'name': 'ticketStatus'
    },
    {
      'dialogCounter': 1,
      'confidence': 0.2192033499479294,
      'name': 'contactNoInfoVerification'
    },
    {
      'dialogCounter': 1,
      'confidence': 0.2100407384335995,
      'name': 'paymentStatus'
    },
    {
      'dialogCounter': 1,
      'confidence': 0.20624047592282296,
      'name': 'getPyamentProcessingInfo'
    }
  ],
  'outputMessages': [
    {
      'raw': {
        'text': [
          'getAcoountNoInfo'
        ],
        'log_messages': [

        ],
        'nodes_visited': [
          'node_9_1557749343317'
        ]
      },
      'message': 'getAcoountNoInfo',
      'type': 'output'
    }
  ],
  'relations': [

  ],
  'entities': [

  ],
  'botRequest': {
    'info': {
      'ConvSessionId': '1558417046437a4f060d7f1c03fd4da56'
    },
    'context': {
      'conversationId': '',
      'topIntents': [

      ],
      'contextElements': [
        {
          'raw': {

          },
          'intents': [

          ],
          'dialogCounter': 0,
          'nodesVisited': [

          ],
          'relations': [

          ],
          'conversationId': u'',
          'entities': [

          ]
        }
      ],
      'entities': [

      ]
    },
    'message': {
      'raw': {

      },
      'message': 'I want to know my account balance',
      'type': 'input'
    },
    'templateBotTag': u'',
    'rawInput': {
      'message': 'I want to know my account balance',
      'tag': u'',
      'data': {

      },
      'context': {

      }
    }
  },
  'context': {
    'conversationId': 'ee990997-f8c0-4572-ae5f-2ecd3fbd12e3',
    'topIntents': [
      {
        'dialogCounters': [
          1
        ],
        'confidence': [
          0.9760584354400634
        ],
        'lastLocDiff': 0,
        'name': 'getAcoountNoInfo',
        'numOccurrence': 1
      }
    ],
    'contextElements': [
      {
        'intents': [

        ],
        'entities': [

        ],
        'dialogCounter': 0,
        'nodesVisited': [

        ],
        'relations': [

        ],
        'conversationId': '',
        'raw': {

        }
      },
      {
        'conversationId': 'ee990997-f8c0-4572-ae5f-2ecd3fbd12e3',
        'entities': [

        ],
        'intents': [
          {
            'dialogCounter': 1,
            'confidence': 0.9760584354400634,
            'name': 'getAcoountNoInfo'
          },
          {
            'dialogCounter': 1,
            'confidence': 0.25793172121047975,
            'name': 'autoPaymentStepsInfo'
          },
          {
            'dialogCounter': 1,
            'confidence': 0.25599955916404726,
            'name': 'General_Ending'
          },
          {
            'dialogCounter': 1,
            'confidence': 0.24558325111865997,
            'name': 'connectToAgent'
          },
          {
            'dialogCounter': 1,
            'confidence': 0.2299309462308884,
            'name': 'lateFeesChargeInfo'
          },
          {
            'dialogCounter': 1,
            'confidence': 0.2289618790149689,
            'name': 'raiseTicket'
          },
          {
            'dialogCounter': 1,
            'confidence': 0.22560224235057832,
            'name': 'ticketStatus'
          },
          {
            'dialogCounter': 1,
            'confidence': 0.2192033499479294,
            'name': 'contactNoInfoVerification'
          },
          {
            'dialogCounter': 1,
            'confidence': 0.2100407384335995,
            'name': 'paymentStatus'
          },
          {
            'dialogCounter': 1,
            'confidence': 0.20624047592282296,
            'name': 'getPyamentProcessingInfo'
          }
        ],
        'dialogCounter': 1,
        'outputMessages': [
          {
            'raw': {
              'text': [
                'getAcoountNoInfo'
              ],
              'log_messages': [

              ],
              'nodes_visited': [
                'node_9_1557749343317'
              ]
            },
            'message': 'getAcoountNoInfo',
            'type': 'output'
          }
        ],
        'raw': {
          'conversation_id': 'ee990997-f8c0-4572-ae5f-2ecd3fbd12e3',
          'system': {
            'dialog_stack': [
              {
                'dialog_node': 'root'
              }
            ],
            'dialog_request_counter': 1,
            'dialog_turn_counter': 1,
            'branch_exited': True,
            'initialized': True,
            '_node_output_map': {
              'node_9_1557749343317': {
                '0': [
                  0
                ]
              }
            },
            'branch_exited_reason': 'completed'
          }
        },
        'nodesVisited': [
          {
            'description': '',
            'name': 'node_9_1557749343317',
            'id': ''
          }
        ],
        'inputMessage': {
          'message': 'I want to know my account balance',
          'type': 'input'
        },
        'relations': [

        ]
      }
    ],
    'entities': [

    ]
  },
  'inputMessage': {
    'message': 'I want to know my account balance',
    'type': 'input'
  }
}


ComponentTray_variable = [
  {
    'componentType': 'APIDATA',
    'message': '',
    'tag': 'createNLPmessage.data',
    'data': [
      {
        'notetypeid': 1,
        'message': 'Your account details for account ending with 7654 as follows.',
        'contentid': 3
      }
    ],
    'name': 'eceb2440-950a-45de-b237-eef8c050cf0d'
  }
]


Input_Request =  {
        "info": {},
        "input": {
          "context":{},
          "data": {},
          "message":"I want to know my account balance",
          "tag": ""
        },
        "requestType": "message",
        "responseType": "json"
      }

output_dict = {
    "info": {
        "ConvSessionId": "15584435297538421f626eb0f02eb21b5"
    },
    "componentTypes": [
        "APIDATA"
    ],
    "context": {
        "conversationId": "5f6abb48-d3f6-4c62-83e6-7bd72dee059a",
        "topIntents": [
            {
                "dialogCounters": [
                    1
                ],
                "confidence": [
                    0.9760584354400634
                ],
                "lastLocDiff": 0,
                "name": "getAcoountNoInfo",
                "numOccurrence": 1
            }
        ],
        "contextElements": [
            {
                "intents": [],
                "entities": [],
                "dialogCounter": 0,
                "nodesVisited": [],
                "relations": [],
                "conversationId": "",
                "raw": {}
            },
            {
                "conversationId": "5f6abb48-d3f6-4c62-83e6-7bd72dee059a",
                "entities": [],
                "intents": [
                    {
                        "dialogCounter": 1,
                        "confidence": 0.9760584354400634,
                        "name": "getAcoountNoInfo"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.25793172121047975,
                        "name": "autoPaymentStepsInfo"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.25599955916404726,
                        "name": "General_Ending"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.24558325111865997,
                        "name": "connectToAgent"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.2299309462308884,
                        "name": "lateFeesChargeInfo"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.2289618790149689,
                        "name": "raiseTicket"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.22560224235057832,
                        "name": "ticketStatus"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.2192033499479294,
                        "name": "contactNoInfoVerification"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.2100407384335995,
                        "name": "paymentStatus"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.20624047592282296,
                        "name": "getPyamentProcessingInfo"
                    }
                ],
                "dialogCounter": 1,
                "outputMessages": [
                    {
                        "raw": {
                            "text": [
                                "getAcoountNoInfo"
                            ],
                            "log_messages": [],
                            "nodes_visited": [
                                "node_9_1557749343317"
                            ]
                        },
                        "message": "getAcoountNoInfo",
                        "type": "output"
                    }
                ],
                "raw": {
                    "conversation_id": "5f6abb48-d3f6-4c62-83e6-7bd72dee059a",
                    "system": {
                        "dialog_stack": [
                            {
                                "dialog_node": "root"
                            }
                        ],
                        "dialog_request_counter": 1,
                        "dialog_turn_counter": 1,
                        "branch_exited": True,
                        "initialized": True,
                        "_node_output_map": {
                            "node_9_1557749343317": {
                                "0": [
                                    0
                                ]
                            }
                        },
                        "branch_exited_reason": "completed"
                    }
                },
                "nodesVisited": [
                    {
                        "description": "",
                        "name": "node_9_1557749343317",
                        "id": ""
                    }
                ],
                "inputMessage": {
                    "message": "I want to know my account balance",
                    "type": "input"
                },
                "relations": []
            }
        ],
        "entities": []
    },
    "APIDATA": [
        {
            "nextCallTag": "",
            "data": [
                {
                    "notetypeid": 1,
                    "message": "Your account details for account ending with 7654 as follows.",
                    "contentid": 3
                }
            ]
        }
    ]
}


