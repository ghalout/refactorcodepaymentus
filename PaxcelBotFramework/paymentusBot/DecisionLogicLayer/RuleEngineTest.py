from RuleEngine import RuleEngine
from Components.WatsonResponseWrapper import __watsonResponse__ as resp
from Actions import *
import json
engine = RuleEngine()
# Pay Bill
output = engine.Execute(resp)
print("Input:")
print(json.dumps(resp, indent=2))
print("##############################################################################")
print("Output")
print(json.dumps(output, indent=2))
print("##############################################################################")

respServiceSelection = resp.copy()
respServiceSelection["intents"] = []
respServiceSelection["entities"] = [{
      "entity": "service",
      "location": [
        16,
        20
      ],
      "value": "phone",
      "confidence": 1
    }]
respServiceSelection["context"] = output["context"]
respServiceSelection["input"]["text"] = "phone"
output = engine.Execute(respServiceSelection)
print("Input:")
print(json.dumps(respServiceSelection, indent=2))
print("##############################################################################")
print("Output")
print(json.dumps(output, indent=2))
print("##############################################################################")

# Pay Bill
resp["entities"][0]["value"] =  "pay"
resp["input"]["text"] = "i would like to pay my bill"
output = engine.Execute(resp)
print("Input:")
print(json.dumps(resp, indent=2))
print("##############################################################################")
print("Output")
print(json.dumps(output, indent=2))
print("##############################################################################")

respServiceSelection = resp.copy()
respServiceSelection["intents"] = []
respServiceSelection["entities"] = [{
      "entity": "service",
      "location": [
        16,
        20
      ],
      "value": "phone",
      "confidence": 1
    }]
respServiceSelection["context"] = output["context"]
respServiceSelection["input"]["text"] = "phone"
output = engine.Execute(respServiceSelection)
print("Input:")
print(json.dumps(respServiceSelection, indent=2))
print("##############################################################################")
print("Output")
print(json.dumps(output, indent=2))
print("##############################################################################")
