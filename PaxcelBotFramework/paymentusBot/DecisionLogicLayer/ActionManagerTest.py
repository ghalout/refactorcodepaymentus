from Components.actionManager import ActionManager


def hello_world(name):
    print("Hello {name}")

actionid = ActionManager.add_action("HelloWorld", hello_world)

ActionManager.call_action("HelloWorld", "Bheem")