import uuid

class __actionManager__:
    __instance__ = None

    def __init__(self):
        self.__actionlist__ = {}
        self.__actionNameMapping__ = {}

    def add_action(self, actionName, func):
        if not callable(func):
            raise Exception("function is not callable")
        
        if actionName in self.__actionlist__:
           raise Exception("action with action name {0} already registered", actionName)
        
        self.__actionlist__[actionName] = func
        actionid = str(uuid.uuid4())
        self.__actionNameMapping__[actionid] = actionName
        return actionid
    
    def remove_action(self, actionid):
        if not actionid in self.__actionNameMapping__:
            raise Exception("no action with action id {0} not found", actionid)
        
        actionName = self.__actionNameMapping__[actionid]
        del self.__actionlist__[actionName]
        del self.__actionNameMapping__[actionid]

    def call_action(self, actionName, args):
        if not actionName in self.__actionlist__:
            raise Exception("action with action name {actionName} not found", actionName)
        
        func = self.__actionlist__[actionName]
        return func(args["data"], args["context"])
    
    def get_action_list(self):
        actionlist = self.__actionlist__.keys()
        return actionlist

    @staticmethod
    def get_instance():
        __instance__ =  __actionManager__.__instance__
        if __instance__ == None:
            __instance__ = __actionManager__()
        return __instance__

ActionManager = __actionManager__.get_instance()



