from DecisionLogicLayer.Components.actionManager import ActionManager

def formatText(data, context):
    return data
def agentCall(data, context):
    return data
def options(data, context):
    return data
ActionManager.add_action("text", formatText)
ActionManager.add_action("agentCall", agentCall)

ActionManager.add_action("options", options)