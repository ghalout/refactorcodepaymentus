
import pg

import datetime



class dbChatlogHandler():


    @staticmethod
    def userConversationTimeDetails(uuid, userid, convstarttime, convendtime,conn_string):

        conn = pg.DB(conn_string)

        result = conn.query('SELECT * FROM insert_userconversationtimedetails($1,$2,$3,$4);',(uuid, userid, convstarttime, convendtime )).getresult()
        # print "result ", result.fetchall()
        #print type(result[0])
        result1 = result[0]
        convId = result1[0]
        print convId
        return convId
        conn.close()


    # message +> function2
    @staticmethod
    def messagesChatlog(message,conn_string):
        conn = pg.DB(conn_string)

        result = conn.query('SELECT * FROM insert_messages($1);',
                            ( message)).getresult()
        # print "result ", result.fetchall()
        # print type(result[0])
        result1 = result[0]
        messageId = result1[0]
        print messageId
        return messageId
        conn.close()

    # seq => function3
    @staticmethod
    def messagesSequenceChatlogs(conversationId, sequenceNo, messageId, isBot,conn_string):
        conn = pg.DB(conn_string)

        result = conn.query('SELECT * FROM insert_sequencedetails($1,$2,$3,$4);',
                            (conversationId, sequenceNo, messageId, isBot)).getresult()

        # result1 = result[0]
        # messageId = result1[0]
        # print messageId
        # return messageId
        conn.close()

    @staticmethod
    def updateUserIdBasedOnchatLogsConvId(userId,chatLogsConvId, conn_string):
        conn = pg.DB(conn_string)

        result = conn.query('SELECT * FROM updateuser_id($1,$2);',
                            (userId, chatLogsConvId)).getresult()

        # result1 = result[0]
        # messageId = result1[0]
        # print messageId
        # return messageId
        conn.close()

        # message +> function2

    @staticmethod
    def chatLogs(convId,leadId,conn_string):
        conn = pg.DB(conn_string)
        print convId


        result = conn.query('SELECT * FROM get_chathistory_conID_usID($1,$2);',
                            (convId, leadId)).getresult()
        # print "result ", result.fetchall()
        # print type(result[0])

        print result
        return result
        conn.close()

    @staticmethod
    def chatLogsofConversation(convId, conn_string):
        conn = pg.DB(conn_string)
        print convId

        result = conn.query('SELECT * FROM get_chathistory_conid($1);',
                            (convId)).getresult()
        # print "result ", result.fetchall()
        # print type(result[0])

        print result
        return result
        conn.close()
