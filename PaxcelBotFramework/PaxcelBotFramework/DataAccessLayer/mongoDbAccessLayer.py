from pymongo import MongoClient
import urllib

class MongoDbHandler(object):

    """

    Class that creates a MongoDb Connection


    """


    def __init__(self,user,host,password,dbName,port):
        mongo_uri = "mongodb://{0}:".format(user) + urllib.quote(password) + "@{0}:{1}/".format(host, port)
        print "url",mongo_uri
        client = MongoClient(mongo_uri)
        self.db = client[dbName]




