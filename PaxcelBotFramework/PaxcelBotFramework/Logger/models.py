import inspect
import json
import logging
import logging.config
import os, sys
#from DataAccessLayer.models import DbHandler
import time


class F_logger():
    def __init__(self, c_path='logging.json', name = __name__):
        try:
            from django.conf import settings

            # Initialising default level to debug

            default_level="DEBUG"
            if settings.LOG_CONFIG:
                path = settings.LOG_CONFIG
            else:
                path = os.path.join(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))), c_path)

            if os.path.exists(path):
                with open(path, 'rt') as f:
                    config = json.load(f)
                if settings.INFO_LOG_PATH:
                    config['handlers']['info_file_handler']['filename'] = os.path.join(settings.INFO_LOG_PATH,"info.log")
                if settings.DEBUG_LOG_PATH:
                    config['handlers']['debug_file_handler']['filename'] = os.path.join(settings.DEBUG_LOG_PATH,"debug.log")
                if settings.ERROR_LOG_PATH:
                    config['handlers']['error_file_handler']['filename'] = os.path.join(settings.ERROR_LOG_PATH,"error.log")
                if settings.LOG_LEVEL:
                    default_level = 'logging.' + settings.LOG_LEVEL
                    config['root']['level'] = settings.LOG_LEVEL
                else:
                    default_level = 'logging.' + config['root']['level']
                logging.config.dictConfig(config)
            print "level of logging: {0}".format(default_level)
            logging.basicConfig(level=default_level)
            self.logger = logging.getLogger(name)

        except ImportError:
            print("Django settings not initialized")
            raise

"""
class D_logger():
    def __init__(self, name = __name__):
        try:
            from django.conf import settings
            if settings.CONVERSATION_STACK:
                self.dbHandler = DbHandler(settings.MONGODB_PARAMETERS["url"],
                                      settings.MONGODB_PARAMETERS["port"],
                                      settings.MONGODB_PARAMETERS["databaseName"]
                                      )

            else:
                logging.getLogger(__name__).debug(
                    "{0}.{1} :: {2}".format(
                        self.__class__.__module__,
                        self.__class__.__name__,
                        "Conversation Stack is not enabled"
                    )
                )

        except ImportError:
            logging.error("Django settings not initialized")
            raise
        except:
            logging.getLogger(__name__).error(
                "{0}.{1} :: {2}".format(
                    self.__class__.__module__,
                    self.__class__.__name__,
                    "::Unexpected error: {0}".format(sys.exc_info()[0])
                )
            )
            raise

    def insert(self,object):
        pass

    def update(self,object):
        pass
"""