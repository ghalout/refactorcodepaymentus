try:
    from models.context import ContextHelperFunctions
except ImportError:
    raise ImportError('unable to import ContextHelperFunctions from models.context file')


try:
    from botTemplate import Ibot
except ImportError:
    raise ImportError('unable to import Ibot in templateBot')


try:
    import binascii
except ImportError:
    raise ImportError('unable to import Ibot in templateBot')

try:
    import os
except ImportError:
    raise ImportError('unable to import os in templateBot')

try:
    import time
except ImportError:
    raise ImportError('unable to import time in templateBot')

try:
    import logging
except ImportError:
    raise ImportError('unable to import time in templateBot')


try:
    import sys
except ImportError:
    raise ImportError('unable to import sys in templateBot')


try:
    import json
except ImportError:
    raise ImportError('unable to import json in templateBot')



logger = logging.getLogger(__name__)



class TemplateBot(Ibot):

    """

    this bot creates BotRequest object from a botTemplate file


    """

    def __init__(self, authenticationParameters=None, name="", botTemplate=None):

        if not authenticationParameters:
            authenticationParameters = {}
            logger.warn("No authentication Parameters detected for template bot")

        if not botTemplate:
            botTemplate = {}
            logger.error("No bot Template file found !!")

        #
        # call to the init method of the base class
        #

        super(TemplateBot, self).__init__(authenticationParameters, name)

        self.botTemplate = botTemplate

    def getResponse(self, botRequest):

        """


        :param botRequest: a "BotRequest" schema which is the users request containing the message and
        :return: "BotResponse" object which constitutes the bots response


        """

        # here the input message is the intent itself

        try:

            savedBotResponse = self.botTemplate[botRequest["templateBotTag"]]

            logger.info(
                "{0}".format(json.dumps(savedBotResponse,indent=2))
            )


            #
            # load intents entities and relations according to the templateBotTag
            #

        except Exception as e:
            logger.error(
                msg="Unable to load templateBotTag ! ",
                exc_info=sys.exc_info(),
                extra={"botRequest": botRequest}
            )
            savedBotResponse = {}


        ##
        ## Initialising all the parameters
        ##

        currentIntents = []
        currentEntities = []
        message = ""

        currentDialogCounter=len(botRequest["context"]["contextElements"])+1

        millis = int(round(time.time() * 1000))
        rand_id = binascii.b2a_hex(os.urandom(10))
        conv_id = str(millis) + str(rand_id)

        #if "Message" in savedBotResponse.keys():
        #    message = savedBotResponse["Message"]

        if "outputMessages" in savedBotResponse.keys():
            outputMessages=savedBotResponse["outputMessages"]
        else:
            outputMessages=[]

        #

        if "inputMessage" in savedBotResponse.keys():
            inputMessage=savedBotResponse["inputMessage"]
        else:
            inputMessage=botRequest["message"]


        if "intents" in savedBotResponse.keys():
            currentIntents=[]
            for intent in savedBotResponse["intents"]:
                intent["dialogCounter"]=currentDialogCounter
                currentIntents.append(intent)

        if "entities" in savedBotResponse.keys():
            for entity in savedBotResponse["entities"]:
                if message:
                    literal = message[entity["location"][0]:entity["location"][1]]
                else:
                    literal = ""

                entity["dialogCounter"]=currentDialogCounter
                entity["literal"]=literal

                currentEntities.append(entity)

        contextNew = botRequest["context"]

        ## Appending the current context element ###

        try:
            conversationId = contextNew["contextElements"][-1]["conversationId"]
            raw={}
            raw["conversation_id"] =conversationId
            raw["system"] = {}
            raw["system"]["dialog_turn_counter"] = currentDialogCounter

        except:
            conversationId = "templateBot_" + str(conv_id)
            raw={}
            logger.warn(
                "No Conversation Id detected fixing the conversation id to {0}".format(conversationId)
            )


        newContextElement = {
            "conversationId": conversationId,
            "entities":currentEntities,
            "relations":[],
            "dialogCounter":currentDialogCounter,
            "raw":raw,
            "intents":currentIntents,
            "outputMessages":outputMessages

        }
        contextNew=ContextHelperFunctions.addContextElement(contextNew,newContextElement)


        response={
            "intents":currentIntents,
            "entities":currentEntities,
            "context":contextNew,
            "botRequest":botRequest,
            "inputMessage":inputMessage,
            "outputMessages":outputMessages


        }



        return response


