# -*- coding: utf-8 -*-
from __future__ import unicode_literals             # line no 1 and this line is for avoiding unicode errors
### Importing different Models ###
import logging
import sys

import copy





logger = logging.getLogger(__name__)

class ContextHelperFunctions():

    ##
    ## A Collection of helper functions to aid in handling of context
    ##

    ##
    ## Bot Functions
    ##

    @staticmethod
    def findIntentOccurrence(context,intentName):
        """

        :param context: context object
        :param intentName: the name of the intent object that we are looking for
        :return:intentOccurences : list of numbers (dialogCounters)

        """
        intentOccurences = []
        try:

            for intent in context["topIntents"]:
                if intent["name"].lower()==intentName.lower():
                    intentOccurences =intent["dialogCounters"]
        except:
            logger.error(
                msg="Error encounterred retreiving intent {0}".format(str(intentName)),
                exc_info=sys.exc_info(),
                extra={"context": context}
            )


        return intentOccurences


    @staticmethod
    def findEntityOccurrence(context,filter=None):

        """

        Note : add supports to include greater than or less than fiterring

        :param filter:{} a filter of key value pairs which we need to match
        :return: a list of [entity] elements
        :return: a list of dialogCounters where these entities occurred

        """

        try:
            if not filter:
                filter = {}
            filterAsSet = set([(key, filter[key]) for key in filter.keys()])
            matched=[]
            dialogCounters=[]
            for ent in context["entities"]:
                entAsSet = set([(key, str(ent[key])) for key in ent.keys()])
                if filterAsSet.issubset(entAsSet):
                    matched.append(ent)
                    dialogCounters.append(ent["dialogCounter"])
        except:
            logger.error(
                msg="Entity Occurrence Failed while applying filter {0} ! ".format(str(filter)),
                exc_info=sys.exc_info(),
                extra={"context": context}
            )
            matched=[]
            dialogCounters=[]

        return matched, dialogCounters

    @staticmethod
    def findIntentCount(context,intentName):
        """

        Find out how many times the intent was the top intent in the conversation

        :param intentName: the name of the intent (str)
        :param context: Dict following schema specified in PaxcelBotFramework.Schemas.schema_context
        :return:count : number

        """

        count=0
        try:
            count=len(ContextHelperFunctions.findIntentOccurrence(context,intentName))
        except:
            logger.error(
                msg="Failed to find intent count ! ",
                exc_info=sys.exc_info(),
                extra={"context": context}
            )


        return count


    @staticmethod
    def findEntityCount(context,filter=None):

        """

        :param filter: specify the filters by which you want to match the entities
        :param context: Dict following schema specified in PaxcelBotFramework.Schemas.schema_context
        :return:count : number

        """

        if not filter:
            filter = {}

        count =len(ContextHelperFunctions.findEntityOccurrence(context,filter))

        return count

    ##
    ## FrameWork Functions
    ##

    @staticmethod
    def addContextElement(orignalContext,contextElement=None):

        """

        :param context: Dict following schema specified in PaxcelBotFramework.Schemas.schema_context
        :param contextElement: context: Dict following schema specified in PaxcelBotFramework.Schemas.schema_contextElement
        :return: context: Dict following schema specified in PaxcelBotFramework.Schemas.schema_context

        """
        ##
        ##
        ## Adds a New Context Element onto the Context Object
        ##
        ##

        """
        previousContext
"context": {
            "conversationId": "",
            "topIntents": [],
            "contextElements": [
                {
                    "raw": {},
                    "intents": [],
                    "dialogCounter": 1,
                    "nodesVisited": [],
                    "relations": [],
                    "conversationId": "",
                    "entities": []
                }
            ],
            "entities": []
        }
        
        
"context": {
        "conversationId": "8f520171-277e-40de-b73c-1c399f5bbcc4",
        "topIntents": [
            {
                "dialogCounters": [
                    1 , 5
                ],
                "confidence": [
                    1 , 1
                ],
                "lastLocDiff": 4,
                "name": "Greetings",
                "numOccurrence": 2
            },
            {
                "dialogCounters": [
                    2
                ],
                "confidence": [
                    1 
                ],
                "lastLocDiff": 0,
                "name": "Bye",
                "numOccurrence": 1
            }
        ],
        "contextElements": [
            {
                "intents": [],
                "entities": [],
                "dialogCounter": 1,
                "nodesVisited": [],
                "relations": [],
                "conversationId": "",
                "raw": {}
            },
            {
                "conversationId": "8f520171-277e-40de-b73c-1c399f5bbcc4",
                "entities": [],
                "intents": [
                    {
                        "dialogCounter": 1,
                        "confidence": 1,
                        "name": "Greetings"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0,
                        "name": "getEmployeesAmbiguityProjectTechnologyDomain"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0,
                        "name": "getAllClients"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0,
                        "name": "getLocationProjects"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0,
                        "name": "getAllProjects"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0,
                        "name": "getTechnologyEmployees"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0,
                        "name": "confirmEmployeeAmbiguityTechnologyProjectDomain"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0,
                        "name": "getProjectsAmbiguityDomainVsTechnology"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0,
                        "name": "isThisAThis"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0,
                        "name": "getProjectInfo"
                    }
                ],
                "dialogCounter": 1,
                "outputMessages": [
                    {
                        "raw": {
                            "text": [
                                "Greetings"
                            ],
                            "log_messages": [],
                            "nodes_visited": [
                                "Welcome"
                            ]
                        },
                        "message": "Greetings",
                        "type": "output"
                    }
                ],
                "raw": {
                    "conversation_id": "8f520171-277e-40de-b73c-1c399f5bbcc4",
                    "system": {
                        "dialog_stack": [
                            {
                                "dialog_node": "root"
                            }
                        ],
                        "dialog_request_counter": 1,
                        "dialog_turn_counter": 1,
                        "branch_exited": true,
                        "_node_output_map": {
                            "node_5_1514030821185": [
                                0,
                                0
                            ]
                        },
                        "branch_exited_reason": "completed"
                    }
                },
                "nodesVisited": [
                    {
                        "description": "",
                        "name": "Welcome",
                        "id": ""
                    }
                ],
                "inputMessage": {
                    "message": " hello  ",
                    "type": "input"
                },
                "relations": []
            }
        ],
        "entities": []
    }

        """

        context = copy.deepcopy(orignalContext)
        del orignalContext

        if not contextElement:
            contextElement={
                "intents": [],
                "entities": [],
                "dialogCounter": 0,
                "nodesVisited": [],
                "relations": [],
                "conversationId": "",
                "raw": {}
            }



        if contextElement["dialogCounter"] >0:
            context["conversationId"]=contextElement["conversationId"]
            if context["conversationId"] != contextElement["conversationId"]:
                logger.warn(msg = "Old conversation Id does not match new conversation id ! {0} != {1} ".format(context["conversationId"],context["contextElement"]["conversationId"]),
                            #exc_info=sys.exc_info(),
                            extra={"oldConversationId" : context["conversationId"],"newConversationId":contextElement["conversationId"]})

            try:
                if "contextElements" in context.keys():
                    # contextElements already exist
                    context["contextElements"].append(contextElement)
                else:
                    # contextElement added for first time
                    context["contextElements"]=[contextElement]

            except:
                logger.error(
                    msg="Failed to update context element in context ! ",
                    exc_info=sys.exc_info(),
                    extra={"context": context}
                )

            """
            
                Code for updating the context element
            
            """
            try:
                if "topIntents" not in context.keys():
                    context["topIntents"]=[]

                if contextElement["intents"]:
                    topIntName=contextElement["intents"][0]["name"]
                    topIntConfidence=contextElement["intents"][0]["confidence"]
                    topIntDialogCounter=contextElement["dialogCounter"]

                    if topIntName:
                        # to avoid blank intents
                        updated=False
                        for i,intent in enumerate(context["topIntents"]):
                            if intent["name"].lower()==topIntName.lower():
                                # Intent has already occurred in the dialog
                                context["topIntents"][i]["dialogCounters"].append(topIntDialogCounter)
                                context["topIntents"][i]["confidence"].append(topIntConfidence)
                                context["topIntents"][i]["numOccurrence"] += 1
                                context["topIntents"][i]["lastLocDiff"] = context["topIntents"][i]["dialogCounters"][-1]-context["topIntents"][i]["dialogCounters"][-2]
                                updated=True

                        if not updated:
                            # Intent encountered for the first time
                            context["topIntents"].append({

                                                          "name":topIntName,
                                                          "dialogCounters":[topIntDialogCounter],
                                                          "confidence":[topIntConfidence],
                                                          "numOccurrence":1,
                                                          "lastLocDiff":0

                                                          })
            except:
                logger.error(
                            msg="Failed to update topIntents in context ! ",
                            exc_info=sys.exc_info(),
                            extra={"context": context}
                            )

            if "entities" not in context.keys():
                context["entities"]=[]

            ###
            ### Updating entities
            ###
            try:
                context["entities"].extend(contextElement["entities"])
            except:
                logger.error(
                    msg="Failed to update entities in context ! ",
                    exc_info=sys.exc_info(),
                    extra={"context": context}
                )

        else:
            # first time context added
            context["contextElements"]=[contextElement]


        return context



















