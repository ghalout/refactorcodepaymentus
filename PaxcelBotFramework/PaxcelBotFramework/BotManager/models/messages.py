import re
import logging

logger = logging.getLogger(__name__)


class MessageHelper():

    """


    A Helper Class for message objects


    """

    @staticmethod
    def substituteEntities(message, entities):

        """

        ##
        ## Substitute  entity literals in message with entity names
        ##
        ## Conflicts may arise in case two entities overlap, currently the longest entity will be substituted first
        ##
        ## Note : Write Better function to resolve this conflict, also test this thoroughly
        ##

        :param entities: A list of dict entity objects following the schema in PaxcelBotFramework.Schema.schema_entities
        :param message: A  message dict object following the schema in PaxcelBotFramework.Schema.schema_message
        :param returns : A message object

        converts " My name is Vaibs , I am an emp "
        to " My name is Vaibhav Arora , I am an Employee "
        (substitutes the literal for the name)


        Conflict detail

        Management   :   MM
        Agile Project Management : Drutas
        Project : PP
        Management Suite : MS

        I want to know about Agile Project Management Suite

        entities extracted :
        1) Agile Project Management
        2) Management
        3) Project
        4) Management Suite








        """

        if message["type"].lower() == "output":
            logger.warn(msg="Output Message Type does'nt support this ! messageEntitesSubstituted wont be changed in message object {0}!!".format(str(message)))
        else:

            ##
            ## Sorts the entities in descending order of Literal Length
            ##

            if entities:
                entities.sort(reverse=True, key=lambda x: x["literalLength"])

            entitesSubstituted = message["message"]
            for entity in entities:
                try:
                    ## replace entities

                    entitesSubstituted = re.sub('\b' + entity["literal"] + '\b', '\b' + entity["name"] + '\b', entitesSubstituted)

                except:
                    pass

        return message



