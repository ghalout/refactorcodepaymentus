### libraries for extracting relations ###
from nltk import RegexpParser
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk import pos_tag
from abc import abstractmethod,ABCMeta              # for abstract classes
#from messages import Message
#from intents import Intent
#from entities import Entity

#from dialogNodes import DialogueNode


##### Relations #####

class Relation():
    """

    An Abstract Model for Bot Relation

    """

    __metaclass__ = ABCMeta

    def __init__(self,name="",computeRelation=True,params={}):




        """

        :param name: name of the relation
        :param computeRelation: whether to compute this relation
        :param params: parameters for the relations


        """
        self.name = name
        self.params={}
        self.relation={} # initialising the relation
        self.computeRelation=computeRelation

    @abstractmethod
    def extractRelation(self):

        """

        update the extracted relation here


        """
        pass

    def toDict(self):
        return {}


class NullRelation(Relation):

    """

    an empty relation class, can be used to default relations

    """

    def __init__(self,name="NullRelation",computeRelation=False,params={}):

        """

        :param name: name of the relation defaults to Null relation
        :param params:{}

        {

        message: Message object,
        entities: A list of entities,
        computeMessage: a flag to see if message is to be computed


        }

        put in checks to check for if the params are not of the desired type

        """
        # put in checks to check for params type when doing exception handling

        super(NullRelation, self).__init__(name=name, params=params,computeRelation=computeRelation)
        self.definedForBotType = 'Bot'


    def extractRelation(self):
        """

        update the extracted relation here


        """
        pass

    def toJson(self):
        return {}


class AndOrBetweenEntityRelation(Relation):
    """

    A General relation which is defined for all  Bots extracts and and or between

    """

    def __init__(self,name="AndOrBetweenEntityRelation",computeRelation=True,params={'message':Message(),"entities":[Entity()],"computeMessage":False}):

        """

        :param name: name of the relation defaults to andOrBetweenEntityRelation
        :param params:

        {

        message: Message object,
        entities: A list of all entities, (leave blank [] if you want them to be calculated)
        computeMessage: a flag to see if message is to be computed
        ignoredEntityTypes: a list of entity types to be ignored for this relation calculation
        ignore by

        }

        put in checks to check for if the params are not of the desired type

        """
        # put in checks to check for params type when doing exception handling

        super(AndOrBetweenEntityRelation, self).__init__(name=name, params=params,computeRelation=computeRelation)
        self.definedForBotType = 'Bot'



    def extractRelation(self):

        if self.computeRelation:

            message= self.params["message"]
            entities=self.params["entities"]
            computeMessage=self.params["computeMessage"]
            ignoredEntityTypes=self.params["ignoredEntityTypes"]

            if computeMessage:
                message.substituteEntities(entities)

            ## print "handleQueryStructureLogicalOperators()",message,entityInfoDict

            grammar = """
                                 GRAMMAR1: {<ENTITY><CC|,><ENTITY>}
                                 GRAMMAR2: {<GRAMMAR1|ENTITY|GRAMMAR2>(<CC><GRAMMAR1|ENTITY|GRAMMAR2>)+}
                                 GRAMMAR3: {<ENTITY>}
                               """
            grammarName = "GRAMMAR1"

            ### preprocess message according to context

            replacements = [
                [ent.literal, "_".join(ent.name.split(" ")), ent.type] for ent
                in entities if ent.type not in ignoredEntityTypes] # if "knowAbout" not in ent["entity"] remember to not include this entity in crmbot

            # # print "replacements",replacements # 'replace x[0] with x[1]'

            valueEntityDict = dict([[elem[1], elem[2]] for elem in replacements])

            for row in replacements:
                message = message.message.replace(row[0], row[1])

            nerOfInterest = [ent.name.replace(" ", "_") for ent in entities if ent.type not in ignoredEntityTypes ]

            #nerOfInterest = set([el.replace(" ", "_") for elem in nerOfInterest for el in elem])
            # # print "nerOfInterest",nerOfInterest


            wordsList = word_tokenize(message)
            tagged = pos_tag(wordsList)

            tagged2 = []
            for elem in tagged:
                if elem[0] in nerOfInterest:
                    tagged2.append((elem[0], 'ENTITY'))
                else:
                    tagged2.append(elem)
            tagged = tagged2
            # # print "tagged",tagged

            # now setting the tags to NET

            chunked = AndOrBetweenEntityRelation.findGrammar(tagged, grammar)

            extractedTreesG1 = AndOrBetweenEntityRelation.extractTree(chunked)

            # find unique elements
            extractedTreesNew = []
            for el in extractedTreesG1:
                if el not in extractedTreesNew:
                    extractedTreesNew.append(el)

            # remove subelements eg python and java is already included in python and java or javascript
            common = []
            for i, el in enumerate(extractedTreesNew):
                eljs1 = "".join([elem[0] for elem in el])
                for j, el2 in enumerate(extractedTreesNew):
                    eljs2 = "".join([elem[0] for elem in el2])
                    if eljs1 in eljs2 and i != j:
                        print "match", eljs1, eljs2
                        common.append(i)

            print "Extracted trees"
            print extractedTreesNew

            toBeRemovedList = [extractedTreesNew[c] for c in common]

            extractedTreesNew = filter(lambda a: a not in toBeRemovedList, extractedTreesNew)

            print extractedTreesNew

            grammer1results = []

            for elem in extractedTreesNew:
                dumlist = []

                for el in elem:
                    if el[1] != 'CC':
                        dumlist.append([el[0], valueEntityDict[el[0]]])
                    else:
                        dumlist.append([el[0]])

                grammer1results.append(dumlist)

            # if a single element exists say [java] then convert it to double [java][and][java]
            print grammer1results
            grammerResultsFinal = []
            for el in grammer1results:
                if len(el) == 1:
                    dumlist = []
                    dumlist.append([el[0][0], el[0][1]])
                    dumlist.append(['and'])
                    dumlist.append([el[0][0], el[0][1]])
                    grammerResultsFinal.append(dumlist)
                else:
                    grammerResultsFinal.append(el)

            print "\n" * 10
            print "finally", grammerResultsFinal
            print "\n" * 10

            self.relation = {"relation":grammerResultsFinal}


    @staticmethod
    def findGrammar(sentence, grammar):
        cp = RegexpParser(grammar)
        result = cp.parse(sentence)
        return result


    @staticmethod
    def extractTree(treeList):
        trees = []
        for elem in treeList.subtrees():
            if elem.label() == 'GRAMMAR2' or elem.label() == 'GRAMMAR1' or elem.label() == 'GRAMMAR3':
                trees.append(elem.leaves())

        return trees
