class EngineInterface:
    """description of class"""

    def __init__(self, config):
        if config == None:
            raise Exception("Invalid Engine cofiguration")

        # if config.agentBotConfig == None:
        #     raise Exception("Agent bot config is null")
        # self.__agentBotConfig__ = config.agentBotConfig
        
        self.__getDestinationBot__ = config["getDestinationbot"]
        self.__assistant__ = config["assistant"]

    def Execute(self, request):
        context = request["context"]
        payload = {
            "workspace_id": self.__getDestinationBot__(context),
            "context": context,
            "input": request["input"]
        }
        response = self.__assistant__(payload)
        context = response["context"]
        if self.__isRedirect__(context):
            payload["workspace_id"] = self.__getDestinationBot__(context)
            payload["context"]["destination_bot"] = context["destination_bot"]
            response = self.__assistant__(payload)

        return response

    def __isRedirect__(self, context):
        if context != None and "redirect" in context and  context["redirect"] == True:
            return True
        else:
            return False

