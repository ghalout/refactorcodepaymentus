from watson_developer_cloud import ConversationV1

class Assistant:
    """ This is a class description"""
    def __init__(self, config):
        self.__assistant__ = ConversationV1(username=config["username"],
                                            password= config["password"],

                                            version=config["version"])
        
    def Message(self, payload):
        response = self.__assistant__.message(workspace_id= payload["workspace_id"],
                                              message_input = payload["input"],
                                              alternate_intents=False,
                                              context = payload["context"]
                                              )
        return response

