try:
    from watson_developer_cloud import ConversationV1  # Support for accessing watson conversation api
except ImportError:
    raise ImportError('unable to import ConversationV1 in watsonBot')
try:
    from NLPEngine import EngineInterface
except ImportError:
    raise ImportError('Unable to import EngineInterface from NLPEngine')
try:
    from NLPEngine import Assistant
except ImportError:
    raise ImportError('Unable to import Assistant from  NLPEngine')

try:
    from models.context import ContextHelperFunctions
except ImportError:
    raise ImportError('unable to import ContextHelperFunctions from models.context in watsonBot')

try:
    import json
except ImportError:
    raise ImportError('unable to import json in watsonBot')

try:
    import uuid
except ImportError:
    raise ImportError('unable to import uuid in watsonBot')

try:
    import botTemplate
except ImportError:
    raise ImportError('unable to import botTemplate in watsonBot')

try:
    from models.messages import MessageHelper
except ImportError:
    raise ImportError('unable to import models.messages in watsonBot')

try:
    from botTemplate import Ibot
except ImportError:
    raise ImportError('unable to import Ibot from factories in watsonBot')

try:
    import logging
except ImportError:
    raise ImportError("unable to import logging")

logger = logging.getLogger(__name__)

__BotWorkspaceIDs__ = {

}


def __getDestinationBot__(context):
    destination_bot = "AGENT"
    if context != None and "destination_bot" in context and context["destination_bot"] != None:
        destination_bot = context["destination_bot"].upper()

    wsId = __BotWorkspaceIDs__[destination_bot]
    if wsId == None:
        wsId = __BotWorkspaceIDs__["AGENT"]

    if destination_bot == None:
        destination_bot = "agent"

    return wsId


class WatsonBot(Ibot):
    """
    The Watson Bot Class which defines how to access
    the watson api and get its response, inherits from the "Bot" class

    """

    def __init__(self, authenticationParameters, name="WatsonBot_" + str(uuid.uuid4())):

        """

        :param authenticationParameters:

                the authentication parameters needed to access the bot

                {
                    'username' : '212e8b97-d3c7-4edf-a759-1150b32e3d87',
                    'password' : 'MzlBgFHfA1lS',
                    'version' :  '2017-06-12',
                    'workSpaceId' :'c1c8ed6a-5955-4b12-8a01-b5bd6813495c'

                }

        :param name: The name of the bot , defaults to WatsonBot_uniqueIdForBot


        """

        ##
        ## Call to the init method of the base class
        ##

        super(WatsonBot, self).__init__(authenticationParameters, name)
        self.username = authenticationParameters["username"],
        self.password = authenticationParameters["password"],
        self.version = authenticationParameters["version"]
        self.workSpaceId = authenticationParameters["workSpaceId"]

    @staticmethod
    def prepareNLPContext(context):
        nlpContext = {}
        if context:
            if "destination_bot" in context:
                nlpContext["destination_bot"] = context["destination_bot"]

        return nlpContext

    def getResponse(self, botRequest, BotWorkspaceIDs):

        """


        :param botRequest: a "BotRequest" type schema which is the users request containing the message and
        :return: "BotResponse" type json schema which constitutes the bots response


        """

        ##                                                               ##
        ##  Extract messageBody and messageContext from the botRequest   ##
        ##                                                               ##

        __BotWorkspaceIDs__.update(BotWorkspaceIDs)
        logger.debug("botRequest 1 as supplied in getResponse Method of watson bot , {0}".format(
            json.dumps(botRequest["context"], indent=2)))

        messageBody, messageContext, computeRelations, origContext = WatsonBot.parseBotRequest(botRequest)

        logger.debug("botRequest 2 after parsing  using the parseBotRequest Method of watson bot  , {0}".format(
            json.dumps(botRequest["context"], indent=2)))

        # conversationObject = ConversationV1(
        #     username=self.username[0],
        #     password=self.password[0],
        #     version=self.version
        # )
        originalContext = botRequest["context"]
        contextVar = WatsonBot.prepareNLPContext(originalContext)
        watson = Assistant({
            'username': self.username[0],
            'password': self.password[0],
            'version': self.version
        })

        nlpEngine = EngineInterface({
            "assistant": watson.Message,
            "getDestinationbot": __getDestinationBot__
        })
        watsonResponse = nlpEngine.Execute({
            "context": contextVar,
            "input": {
                "text": messageBody
            }
        })
        # watsonResponse=conversationObject.message(workspace_id=self.workSpaceId, message_input=messageBody,
        #                           alternate_intents=True, context=messageContext)

        ##
        ## Format the watsonResponse to BotResponse
        ##

        # print "The watson Json Returned"
        # # print json.dumps(watsonResponse,indent=2)
        #
        # logger.debug("botRequest 3 after calling the watson api   , {0}".format(
        #     json.dumps(botRequest["context"], indent=2)))
        #
        # botResponse = WatsonBot.formatBotResponse(watsonResponse, computeRelations, origContext, botRequest)
        #
        # logger.debug("botRequest 9 after calling the watsonBot.formatBotResponse method  , {0}".format(
        #     json.dumps(botResponse["botRequest"]["context"], indent=2)))
        intents = watsonResponse["intents"]
        entities = watsonResponse["entities"]
        nlpContext = watsonResponse["context"]
        if "destination_bot" in nlpContext:
            originalContext["destination_bot"] = nlpContext["destination_bot"]
        resp = {
            "intents": intents,
            "entities": entities,
            "context": originalContext,
            "botRequest":botRequest
        }
        return resp



    ##########################################################################################
    ############################### Input Output Formatting ##################################
    ##### Input Formating #####

    @staticmethod
    def parseBotRequest(botRequest):

        ##
        ## Extract data from botRequest to format accepted by watson
        ##

        messageBody = WatsonBot.parseInputMessage(botRequest["message"])

        logger.debug("Message returned from watson  {0} ".format(messageBody))

        ##
        ## parse the bot context here
        ##
        watsonContext = WatsonBot.parseInputContext(botRequest["context"])

        ## orignal Context
        origContext = botRequest["context"]

        ## the relations that have to be computed at this stage
        ## to be added later / can be added later
        try:
            computeRelations = botRequest["computeRelations"]
        except:
            computeRelations = {}
            logger.warn("No relations specified")

        logger.debug("Watson Message {0}".format(messageBody))
        logger.debug("Watson Context {0}".format(watsonContext))
        logger.debug("Type {0}".format(type(watsonContext)))

        return messageBody, watsonContext, computeRelations, origContext

    @staticmethod
    def parseInputContext(context):

        ##
        ## In order to modify/extract the context before sending to the bot
        ##

        if "contextElements" in context.keys():
            lastContextElem = context["contextElements"][-1]
            cont = lastContextElem["raw"]
        else:
            cont = {}
            logger.info("No context elements specified")

        return cont

    @staticmethod
    def parseInputMessage(message):
        """

        :param message: json for message object
        :return: str as message
        If you want do some preprocessing to the input message
        before sending it to the bot will be done here

        Watson accepts data as

        {

        "text" : "hello"

        }

        """

        messageBody = message["message"]  # required because of watson format

        return messageBody

    #####                      ######
    #####  Output Formatting   ######
    #####                      ######

    @staticmethod
    def formatBotResponse(response, computeRelations, origContext, botRequest):

        ##
        ## Convert the watsonResponse to the BotResponse Format
        ##
        ## Add the default relations to be computed everytime here
        ##

        intents = WatsonBot.extractIntents(response)
        entities = WatsonBot.extractEntities(response)

        logger.debug("In formatBotResponse  now botRequest 4 {0}".format(json.dumps(botRequest["context"], indent=2)))
        ## to do
        inputMessage, outputMessages = WatsonBot.extractInputOutputMessage(response)

        logger.debug("In formatBotResponse now botRequest 5 after extractingInputOutputMessage {0}".format(
            json.dumps(botRequest["context"], indent=2)))
        ## to do
        if entities:
            inputMessage = MessageHelper.substituteEntities(inputMessage, entities)
            # inputMessage.substituteEntities(entities)

        logger.debug("In formatBotResponse now botRequest 6 after extractingInputOutputMessage {0}".format(
            json.dumps(botRequest["context"], indent=2)))

        previousContext = origContext.copy()  #
        botRequestDum = botRequest.copy()  # fix to preserve orignal context
        contextNew = WatsonBot.extractContext(response, previousContext, inputMessage, outputMessages)

        logger.debug("In formatBotResponse now botRequest 7 after extract context {0}".format(
            json.dumps(botRequestDum["context"], indent=2)))

        botResponse = {
            "intents": intents,
            "entities": entities,
            "relations": [],
            "inputMessage": inputMessage,
            "outputMessages": outputMessages,
            "context": contextNew,
            "botRequest": botRequestDum
        }

        logger.debug("In formatBotResponse now botRequest 8 after extract context {0}".format(
            json.dumps(botRequestDum["context"], indent=2)))

        return botResponse

    ###
    ### response parsing helper methods ####
    ###
    @staticmethod
    def computeRelations(computeRelations, entities=None):

        ##
        ## Note that this function is to be editted when we Create relations
        ##

        relations = []
        for relation in computeRelations:
            if relation["computeRelation"]:
                ## handle when relations are added
                pass

        pass

    @staticmethod
    def extractEntities(response):
        """

        Convert
         [
         {
              "confidence": 1,
              "location": [
                9,
                16
              ],
              "value": "Vaibhav Arora",
              "entity": "Employee"
        }
        ]
        object to Entity Object

        my name is karta dharta and i am an employee

        literal = karta dharta

        entity : Employee
        value : Vaibhav Arora , Synonym : vaibhav, vibhu , sda , fw master , karta dharta
        returns list of entity objects captured

        """

        if not response:
            logger.error("No output received from watson !")
            raise Exception("No output received from watson !")

        else:

            ##
            ##
            ## Capture entities from response
            ##
            ##

            dialogCounter = response["context"]["system"]["dialog_turn_counter"]

            if response['entities']:
                message = response["input"]["text"]
                entities = [
                    {
                        "name": entDict["value"],
                        "type": entDict["entity"],
                        "location": entDict["location"],
                        "confidence": entDict["confidence"],
                        "literal": message[entDict["location"][0]:entDict["location"][1]],
                    # what exactly the user is typing
                        "dialogCounter": dialogCounter,
                        "literalLength": len(message[entDict["location"][0]:entDict["location"][1]])
                    }
                    for entDict in response["entities"]
                ]
            else:
                entities = []
            return entities

    @staticmethod
    def extractIntents(response):

        """
        extracts intents from the json returned by watson


        returns list of intent objects captured


        {
                    "conversation_id": "8f520171-277e-40de-b73c-1c399f5bbcc4",
                    "system": {
                        "dialog_stack": [
                            {
                                "dialog_node": "root"
                            }
                        ],
                        "dialog_request_counter": 1,
                        "dialog_turn_counter": 1,
                        "branch_exited": true,
                        "_node_output_map": {
                            "node_5_1514030821185": [
                                0,
                                0
                            ]
                        },
                        "branch_exited_reason": "completed"
                    }
                }
        }


        """

        if not response:
            logger.error("Bot has not run !")
            raise Exception("Bot not run !")
        else:
            # Capture intents from response
            intents = []
            dialogCounter = response["context"]["system"]["dialog_turn_counter"]

            if response['intents']:
                ##
                ## Create a list of intent objects
                ##
                intents = [
                    {
                        "name": intent['intent'],
                        "confidence": intent['confidence'],
                        "dialogCounter": dialogCounter
                    }
                    for intent in response['intents']
                ]

            return intents

    @staticmethod
    def extractInputOutputMessage(response):

        """


        :param response: The watson response (json format)

        :return:

        inputMessage : A message type json schema of input type ,
        outputMessages : A list of output type Message Types


        """

        inputMessage = {"message": response["input"]['text'],
                        "type": 'input'}  # messageBody is in the response["input"]

        outputMessages = [{
            "message": mes,
            "type": 'output',
            "raw": response["output"]
        }

            for mes in response["output"]['text']

        ]

        return inputMessage, outputMessages

    @staticmethod
    def extractContext(response, previousContext, inputMessage, outputMessages):

        """
        extracts context from the waston response and converts it to our schema


        :param response: the returned watson response variable
        :return: A Context type json schema object



        """
        conversationId = response["context"]["conversation_id"]
        dialogCounter = response["context"]["system"]["dialog_turn_counter"]
        nodesVisited = []

        logger.debug("1 in Extract context {0} ,{1}".format(dialogCounter, previousContext))

        try:
            nodesVisited = [{"name": name, "id": "", "description": ""} for name in response["output"]["nodes_visited"]]
            ##
            ## modify this in future
            ##
        except:
            pass

        logger.debug(
            "2 in Extract context after calculating nodes visited {0} ,{1}".format(dialogCounter, previousContext))

        entities = WatsonBot.extractEntities(response)

        logger.debug("3 in Extract context after extracting entities {0} ,{1}".format(dialogCounter,
                                                                                      previousContext))

        intents = WatsonBot.extractIntents(response)
        logger.debug("4 in Extract context after extracting intents {0} ,{1}".format(dialogCounter,
                                                                                     previousContext))
        contextElem = {

            "intents": intents,
            "conversationId": conversationId,
            "entities": entities,
            "nodesVisited": nodesVisited,
            "relations": [],
            "dialogCounter": dialogCounter,
            "raw": response['context'],  # setting watson context in the raw field
            "inputMessage": inputMessage,
            "outputMessages": outputMessages

        }

        logger.debug("5 in Extract context after creating a contextElement {0} ,{1}".format(dialogCounter,
                                                                                            previousContext))

        context = ContextHelperFunctions.addContextElement(previousContext, contextElem)

        logger.debug("6 in Extract context after addContextElement {0} ,{1}".format(dialogCounter,
                                                                                    previousContext))

        return context



