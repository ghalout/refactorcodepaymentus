try:
    import logging
except ImportError:
    raise ImportError("Unable to import logging ")

try:
    import json
except ImportError:
    raise ImportError("unable to import json")

try:
    from ipware.ip import get_ip
except ImportError:
    raise ImportError("ipware module not found")

try:
    from abc import ABCMeta,abstractmethod
except ImportError:

    raise ImportError("Unable to import ABCMeta and Abstractmethod")

try:
    from PaxcelBotFramework.BotManager.models.context import ContextHelperFunctions
except ImportError:
    raise ImportError("Unable to import ContextHelperFunctions from PaxcelBotFramework.BotManager.models.context")

try:
    from yattag import Doc
except ImportError:
    raise ImportError("Unable to import Doc from Yattag check if Yattag is installed")

try:
    from messageClasses import tagsDict
except ImportError:
    raise ImportError("Unable to import messageClasses from tagsDict")

try:
    import binascii
except ImportError:
    raise ImportError("Unable to import binascii ")


try:
    import os
except ImportError:
    raise ImportError("Unable to import os ")


try:
    import time
except ImportError:
    raise ImportError("Unable to import time ")


try :
    from PaxcelBotFramework.Schemas.masterSchema import masterSchema
except ImportError:
    raise ImportError("Unable to import master schema ")


try :
    import jsonschema # for schema validation
except ImportError:
    raise ImportError("Unable to import Json schema" )





logger = logging.getLogger(__name__)

class InputProcessor():

    """

    All the input parsers will have this format, this is the abstract base class that specifies that

    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def extractRequestData(self, request):
        """

        an abstract method to be defined in each of the derived classes
        :param request: request object
        :return:

        """
        pass


class OutputProcessor():

    """

    All the output parsers will have this format, this is the abstract base class that specifies that


    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def processRequest(self):
        """

        an abstract method which is to be defined in each of the derived classes

        :return:
        """
        pass




class SimpleInputProcessor(InputProcessor):

    """

    A class responsible for handling the input request and extracting all the data from it


    """



    def __init__(self):

        self.requestType=""
        self.responseType = ""
        self.input = {   "message":""
                        ,"context":{}
                    }
        ##
        ## In case we need to modify the input message , context  at the python end
        ##  we change keep that below
        ##

        self.message = ""
        self.context=""

        self.info = {}
        self.ip=""
        self.templateBotTag="" # later if you need data for templateBot change it to templateBotData



    def extractRequestDataforFbBot(self,request):


        ##
        ## Duties :
        ## extract the type ,content , ip from the request
        ## update  requestType ,requestContent,ip
        ##
        
        received_json_data = request # request.body : input request json

        ##
        ## Setting the request type and response type
        ##

        self.requestType =received_json_data["requestType"]
        self.responseType=received_json_data["responseType"]
        self.input=received_json_data["input"]
        self.info=received_json_data["info"]

        ##
        ##
        ##  Calculate the Ip Address
        ##
        ##

        ##ip = get_ip(request)
        ##if ip is None:
          ##  ip = "We Dont Have IP Address For This User"
          ##  logger.warn(" No Ip found for this call")
        ##self.ip = ip

        ##
        ##
        ## Extract Message and Context
        ##
        ##

        requestContent = received_json_data["input"]
        try:
            self.context=requestContent["context"]
        except Exception:
            self.context = {}
            logMes=" Context corrupted in input . Got input {0}".format(
                self.input)

            logger.error(logMes)  # modify to log input context
            raise Exception(logMes)


        try:
            self.message=requestContent["message"]
        except :
            logMes = " Input message not found . Got input {0}".format(
                self.input)
            self.message = ""
            logger.warn(logMes)


            #logger.warn(" No message detected therefore setting empty . Got Input {0}".format(self.input))

        ##                                                                                 ##
        ##                                                                                 ##
        ## Generate Unique Conversation Session Id , which is Unique to every Conversation ##
        ##                                                                                 ##
        ##                                                                                 ##

        millis = int(round(time.time() * 1000))
        rand_id = binascii.b2a_hex(os.urandom(10))
        conv_id = str(millis) + str(rand_id)

        if 'ConvSessionId' in self.info.keys():
            if self.info['ConvSessionId'] == "":
                self.info['ConvSessionId'] = conv_id

        else:
            self.info['ConvSessionId'] = conv_id
        try :
            self.templateBotTag=self.input["tag"]
        except:
            self.templateBotTag=""
            logger.warn(" No tag detected therefore setting empty")

    def extractRequestData(self, request):

        ##
        ## Duties :
        ## extract the type ,content , ip from the request
        ## update  requestType ,requestContent,ip
        ##

        received_json_data = dict(json.loads(request.body))  # request.body : input request json

        ##
        ## Setting the request type and response type
        ##

        self.requestType = received_json_data["requestType"]
        self.responseType = received_json_data["responseType"]
        self.input = received_json_data["input"]
        self.info = received_json_data["info"]

        ##
        ##
        ##  Calculate the Ip Address
        ##
        ##

        ip = get_ip(request)
        if ip is None:
            ip = "We Dont Have IP Address For This User"
            logger.warn(" No Ip found for this call")
        self.ip = ip

        ##
        ##
        ## Extract Message and Context
        ##
        ##

        requestContent = received_json_data["input"]
        try:
            self.context = requestContent["context"]
        except Exception:
            self.context = {}
            logMes = " Context corrupted in input . Got input {0}".format(
                self.input)

            logger.error(logMes)  # modify to log input context
            raise Exception(logMes)

        try:
            self.message = requestContent["message"]
        except:
            logMes = " Input message not found . Got input {0}".format(
                self.input)
            self.message = ""
            logger.warn(logMes)

            # logger.warn(" No message detected therefore setting empty . Got Input {0}".format(self.input))

        ##                                                                                 ##
        ##                                                                                 ##
        ## Generate Unique Conversation Session Id , which is Unique to every Conversation ##
        ##                                                                                 ##
        ##                                                                                 ##

        millis = int(round(time.time() * 1000))
        rand_id = binascii.b2a_hex(os.urandom(10))
        conv_id = str(millis) + str(rand_id)

        if 'ConvSessionId' in self.info.keys():
            if self.info['ConvSessionId'] == "":
                self.info['ConvSessionId'] = conv_id

        else:
            self.info['ConvSessionId'] = conv_id
        try:
            self.templateBotTag = self.input["tag"]
        except:
            self.templateBotTag = ""
            logger.warn(" No tag detected therefore setting empty")

        """

        This function converts the data from the input request into a BotRequest Type schema .


        :param BotType: The type of the Bot Used in Bot manager
        :return:  BotRequest Object


        """

        ### Create Message Schema from Input Request Message
        message = {"message": self.message, "type": "input", "raw": {}}  # Message(message=self.message,type="Input")

        # jsonschema.validate(message,schema=masterSchema["message"])

        ###
        ### Initialise Context
        ###

        ##
        ## initialising context to blank if not needed
        ##

        contextFirstTimeCall = {}
        if not self.context:
            ##
            ## For the first call
            ##
            # try:
            #    conversationId=self.context["conversationId"]
            # except:
            conversationId = ""  ## set to none later
            logger.warn(" No conversationId detected therefore setting empty")

            # try:
            #    topIntents=self.context["topIntents"]
            # except:
            topIntents = []
            logger.warn(" No topIntents detected therefore setting empty")

            # try:
            #    entities=self.context["entities"]
            # except:
            entities = []
            logger.warn(" No entities detected therefore setting empty")

            context = {
                "conversationId": conversationId,
                "topIntents": topIntents,
                "entities": entities
            }

            ##print json.dumps(context.toDict(),indent=2)
            """
            contextElement = {
                "conversationId": conversationId,
                "entities":[],
                "nodesVisited":[],
                "relations":[],
                "intents":[],
                "dialogCounter":1.0,
                "raw":{}

            }
            """

            contextFirstTimeCall = ContextHelperFunctions.addContextElement(context)

        # jsonschema.validate(contextFirstTimeCall,schema=masterSchema["context"])

        if contextFirstTimeCall:
            botRequest = {"message": message, "context": contextFirstTimeCall, "templateBotTag": self.templateBotTag,
                          "rawInput": self.input, "info": self.info}
        else:
            botRequest = {"message": message, "context": self.context, "templateBotTag": self.templateBotTag,
                          "rawInput": self.input, "info": self.info}

        # jsonschema.validate(botRequest,schema=masterSchema["botRequest"])

        #
        # print botRequest
        #

        return botRequest



    def createBotRequest(self):

        """

        This function converts the data from the input request into a BotRequest Type schema .


        :param BotType: The type of the Bot Used in Bot manager
        :return:  BotRequest Object


        """

        ### Create Message Schema from Input Request Message
        message={"message":self.message,"type":"input","raw":{}} # Message(message=self.message,type="Input")

        # jsonschema.validate(message,schema=masterSchema["message"])

        ###
        ### Initialise Context
        ###

        ##
        ## initialising context to blank if not needed
        ##

        contextFirstTimeCall={}
        if not self.context:
            ##
            ## For the first call
            ##
            #try:
            #    conversationId=self.context["conversationId"]
            #except:
            conversationId = "" ## set to none later
            logger.warn(" No conversationId detected therefore setting empty")

            #try:
            #    topIntents=self.context["topIntents"]
            #except:
            topIntents=[]
            logger.warn(" No topIntents detected therefore setting empty")


            #try:
            #    entities=self.context["entities"]
            #except:
            entities=[]
            logger.warn(" No entities detected therefore setting empty")

            context={
                "conversationId":conversationId,
                "topIntents":topIntents,
                "entities":entities
            }

            ##print json.dumps(context.toDict(),indent=2)
            """
            contextElement = {
                "conversationId": conversationId,
                "entities":[],
                "nodesVisited":[],
                "relations":[],
                "intents":[],
                "dialogCounter":1.0,
                "raw":{}

            }
            """

            contextFirstTimeCall=ContextHelperFunctions.addContextElement(context)



        #jsonschema.validate(contextFirstTimeCall,schema=masterSchema["context"])

        if contextFirstTimeCall:
            botRequest = {"message":message, "context":contextFirstTimeCall,"templateBotTag":self.templateBotTag ,
                          "rawInput":self.input,"info":self.info}
        else:
            botRequest = {"message": message, "context": self.context, "templateBotTag": self.templateBotTag,
                          "rawInput": self.input, "info": self.info}


        #jsonschema.validate(botRequest,schema=masterSchema["botRequest"])

        #
        # print botRequest
        #

        return botRequest


class OutputProcessorType1(OutputProcessor):

    """


    This Class is responsible for converting the MessageLayerModel object into output response as per the bot specifications


    """

    def __init__(self,outputContent=None,botDisplayName="",responseType="html",uiComponentTypeFunctionMap=None):

        """


        :param outputContent: This the response that we get from the message Formation Layer
        :param botDisplayName: the name that is to be displayed in the bot Icon
        :param responseType:
        :param uiComponentTypeFunctionMap: [("BotBrain",processBotBrain),("BotBrain",processBotBrain)]



        """



        if not outputContent:
            outputContent={}


        self.outputContent  = outputContent
        self.botDisplayName = botDisplayName
        self.responseType   = responseType
        self.info={}

        if not uiComponentTypeFunctionMap:
            uiComponentTypeFunctionMap={}
            logger.warn(" No uiComponentTypeFunctionMap detected therefore setting empty")

        ##
        ##
        ## loading pre-existing function maps
        ##
        ##

        self.uiComponentTypeFunctionMap = {
            "SimpleMessage": OutputProcessorType1.processSimpleMessage,
            "Bubble": OutputProcessorType1.processBubble,
            "FeedBack":OutputProcessorType1.processFeedBack,
            "CallBackForm":OutputProcessorType1.processCallBackForm,
            "AuthForm": OutputProcessorType1.processAuthForm,
            "TextArea":OutputProcessorType1.processTextArea
        }

        for dataType in uiComponentTypeFunctionMap.keys():
            functionMap=uiComponentTypeFunctionMap[dataType]
            self.uiComponentTypeFunctionMap[dataType]=functionMap

        self.outputDict={} # the final output



    def processRequest(self):

        """

        This function processes the contents of the MessageLayerModel and creates an output json according
        to each object

        :return: outputDict dictionary which when converted to json is accepted by the frontend (the final output)

        """

        componentTypes=set() # so that we have unique component Types
        for uiComponent in self.outputContent["uiComponents"]:
            # Simple Code to Extract the string name of the class
            uiComponentType=uiComponent["componentType"]
            #str(type(uiComponent)).replace("<class ","").replace(">","").replace("'","").split(".")[-1]
            try:
                componentTypes.add(uiComponentType)

                self.uiComponentTypeFunctionMap[uiComponentType](self,uiComponent)
                #
                # for componentType = "SimpleMessage"
                # self.uiComponentTypeFunctionMap[uiComponentType] = OutputProcessorType1.processSimpleMessage
                # for componentType = "Bubble"
                # self.uiComponentTypeFunctionMap[uiComponentType] = OutputProcessorType1.processBubble
                #

            except:
                logger.warn(msg="Warning - Component Type {0} Not Found !!".format(uiComponentType),extra={ "outputContainer": self.outputContent["uiComponents"] })


        self.outputDict["context"]=self.outputContent["botResponse"]["context"]

        ##
        ## updating componentTypes types
        ##

        self.outputDict["componentTypes"]=list(componentTypes)
        self.outputDict["info"]=self.outputContent["botResponse"]["botRequest"]["info"]

        return self.outputDict


    ##
    ##
    ## Element Mapper Functions
    ##
    ## To Add custom Mapper Functions
    ##
    ## Any custom mapper function will have responseElement as the input
    ## also so that the custom mapper function is loaded ensure to add it in the uiComponentTypeFunctionMap
    ## uiComponentTypeFunctionMap =[["elementClassType","functionInstance"]]
    ##
    ##
    ##


    ###
    ### Element Mapper functions for SimpleMessage ###
    ###
    def processSimpleMessage(self,responseElement):

        if "SimpleMessage" in self.outputDict.keys():
            if "html" in self.responseType :
                 self.outputDict["SimpleMessage"].append(
                  OutputProcessorType1.createHtmlSimpleMessage(responseElement.message, self.botDisplayName))
            elif "json" in self.responseType :
                self.outputDict["SimpleMessage"].append(responseElement["message"])

        else:
            if  "html" in self.responseType :
                 self.outputDict["SimpleMessage"] = [
                     OutputProcessorType1.createHtmlSimpleMessage(responseElement.message, self.botDisplayName)]
            elif "json" in self.responseType :
                self.outputDict["SimpleMessage"] = [responseElement["message"]]


    @staticmethod
    def  createHtmlSimpleMessage(message,botDisplayName):

        """
        this is depreciated
        this function creates the html for a simple message object
        We have used the Yattag library to create HTML

        :param message: the message to be displayed
        :param botDisplayName: the bot name to be displayed in the div object
        :return: the created html

        """

        doc, tag, text = Doc().tagtext()

        with tag('div', klass=tagsDict.floatleft):
            with tag('div', klass=tagsDict.fontWeightSize):
                doc.stag('img',
                         ('alt', botDisplayName),
                         ('src', tagsDict.botImageUrl),
                         ('height', tagsDict.botImageHeight),
                         ('width', tagsDict.botImageWidth),
                         ('title', botDisplayName)
                         )

                text(botDisplayName)
            with tag('div', klass=tagsDict.chatMessage):
                with tag('p', klass=tagsDict.messageText):
                    text(message)

        return str(doc.getvalue()).replace('"',"'")


    ###
    ### Element Mapper functions for Bubble ###
    ###
    def processBubble(self,bubble):
        if "Bubble" in self.outputDict.keys():
            # if self.responseType=="json"
            self.outputDict["Bubble"].append({"message": bubble["message"], "uiReturnData": bubble["uiReturnData"]})
        else:
            self.outputDict["Bubble"]=[{"message": bubble["message"], "uiReturnData": bubble["uiReturnData"]}]


    ###
    ### Element Mapper functions for AuthForm ###
    ###
    def processAuthForm(self, responseElement):
        self.outputDict["AuthForm"] = {}

    ###
    ### Element Mapper functions for FeedBack ###
    ###
    def processFeedBack(self, responseElement):
        self.outputDict["FeedBack"] = {}

    ###
    ### Element Mapper functions for CallBackForm ###
    ###
    def processCallBackForm(self, responseElement):
        self.outputDict["CallBackForm"] = {}


    ###
    ### Element Mapper functions for TextArea ###
    ###
    def processTextArea(self,textAreaElement):
        if "TextArea" in self.outputDict.keys():
            self.outputDict["TextArea"].append({"message": textAreaElement["message"], "placeHolder": textAreaElement["placeHolder"]})
        else:
            self.outputDict["TextArea"]=[{"message": textAreaElement["message"], "placeHolder": textAreaElement["placeHolder"]}]















