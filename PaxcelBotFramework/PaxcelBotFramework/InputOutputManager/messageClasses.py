from PaxcelBotFramework.HelperTools.models import dotDict

tagsDict=dotDict(
    {
"floatleft": "left", # set position of div

"fontWeightSize": "author-name", # set position of kai and images

"contextBox":"scrollbar-contextbox", # set width, position ,height, and overflow of bot brain window

"orderList":"orderList",  #  css for botbrain list

"listItem":"listItem", # css for botBrain list item font and list style

"botImageUrl": "assets/img/bot-profile-icon.png",  # path for kai image

"chatMessage":"chat-message" ,# chat message

"botImageHeight":"38px",

"botImageWidth":"38px",

"messageText":"message-text"

}

)