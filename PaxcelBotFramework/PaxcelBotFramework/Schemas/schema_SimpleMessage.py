schema = {

  "type": "object",

  "properties": {

    "data":{"type":"object"},    # this data can be used in the Message formation layer to create message

    "message":{"type":"string"}, # this is basically a string value that is shown on the UI

    "tag":{"type":"string"},     # this is the tag which will be used in the message formation layer

    "componentType":{"type":"string",
          "patternProperties": {
                "progBinaryName": "SimpleMessage"
            }

            },                   # this property is common to all types of UiComponents rest may change

    "name":{"type":"string"}     # any string value can be used as a name

  }

}

