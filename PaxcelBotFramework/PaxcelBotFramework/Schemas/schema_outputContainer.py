from schema_CallBackForm import schema as CallBackForm
from schema_AuthForm import schema as AuthForm
from schema_Feedback import schema as Feedback
from schema_SimpleMessage import schema as SimpleMessage
from schema_Bubble import schema as Bubble
from schema_botResponse import schema as botResponse
from schema_UIComponentSchema import  schema as UIComponentSchema

##
## import jsonschema
##

schema={
    "type":"object",
    "properties":{
        "botResponse" : {"$ref": "#/definitions/botResponse"},
        "uiComponents" :{
            "type":"array",
            "contains":{
                        "anyOf":[

                                 {"$ref": "#/definitions/CallBackForm"},
                                 {"$ref": "#/definitions/AuthForm"},
                                 {"$ref":"#/definitions/Feedback"},
                                 {"$ref":"#/definitions/SimpleMessage"},
                                 {"$ref":"#/definitions/Bubble"},
                                 {"$ref":"#/definitions/UIComponentSchema"}

                                 ]
            },
            "minItems":0,
            "items":{"anyOf":[
                                 {"$ref": "#/definitions/CallBackForm"},
                                 {"$ref": "#/definitions/AuthForm"},
                                 {"$ref":"#/definitions/Feedback"},
                                 {"$ref":"#/definitions/SimpleMessage"},
                                 {"$ref":"#/definitions/Bubble"},
                                 {"$ref":"#/definitions/UIComponentSchema"}

                                 ]
        }

    },

},
"definitions":{

    "CallBackForm":CallBackForm,
    "AuthForm":AuthForm,
    "Feedback":Feedback,
    "SimpleMessage":SimpleMessage,
    "Bubble":Bubble,
    "botResponse":botResponse,
    "generalSchema":UIComponentSchema
}

}

"""
schema = {
    "type": "object",
    "properties": {
        "conversationId": {"type":"string"},
        "contextElements":{
            "type":"array",
            "contains":{
                        "anyOf":[{"$ref": "#/definitions/intent"},{"$ref": "#/definitions/entity"},{"type":"number"}]
            },
            "minItems":0,
            "items":{"anyOf":[{"$ref": "#/definitions/intent"},{"$ref": "#/definitions/entity"},{"type":"number"}],



        }

    }},
    "definitions":{
        "intent":{"type":"object"},
        "entity":{"type":"string"}
    }
}
obj1={
    "conversationId":"asdf",
    "contextElements":["asdfasdf",{"hey":"asdf"},"asdf",1]

}
import jsonschema
jsonschema.validate(obj1,schema=schema)
"""


