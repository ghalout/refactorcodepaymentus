from schema_context import schema as contextSchema
from schema_message import schema as messageSchema
from schema_relations import schema as relationSchema

"""
{
                    "message":self.message.toDict(),
                    "context":self.context.toDict(),
                    "computeRelations":[el.toDict() for el in self.computeRelations],
                    "rawInput":self.rawInput,
                    "templateBotTag":self.templateBotTag,
                    "info":self.info
        }
"""

schema = {
  "type": "object",
  "properties": {
    "message": {"$ref": "#/definitions/messageSchema"}, #messageSchema,
    "context": {"$ref": "#/definitions/context"},
    "rawInput": {"type":"object"},
    "computeRelations":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/relation"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/relation"}

        },
    "templateBotTag":{"type":"string"},
     "info":{"type":"object"}
  },
  "definitions":{
      "context":{
    "type": "object",
    "properties": {
        "conversationId": {"type":"string"},
        "contextElements":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/contextElement"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/contextElement"}

        },
        "entities":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/entity"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/entity"}
        },
        "topIntents":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/topIntentSchema"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/topIntentSchema"}

        },


}},
      "relation":{

  "type": "object"

},
      "messageSchema":{
                  "type": "object",
                  "properties": {
                    "message": {"type":"string"},
                    "type": {"type":"string"},
                    "entitesSubstituted": {"type":"string"},
                    "dialogCounter": {"type":"number"},
                    "raw":{"type":"object"}
                  },
                  "required":["message","type","raw"] #"entitesSubstituted","dialogCounter",
                },
      "contextElement": {
    "type": "object",
    "properties": {
        "conversationId": {"type":"string"},
        "intents":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/intent"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/intent"}

        },
        "entities":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/entity"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/entity"}

        },
        "nodesVisited":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/dialogNode"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/dialogNode"}

        },
        "raw":{
            "type":"object"

        },
        "dialogCounter":{"type":"number"}
    },

    "required":["conversationId","intents","entities","nodesVisited","raw","dialogCounter"]
},
        "intent":{

  "type": "object",
  "properties": {
    "name": {"type":"string"},
    "confidence": {"type":"number"},
    "dialogCounter": {"type":"number"}
  },
  "required":["name","confidence","dialogCounter"]
},
        "entity":{
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "confidence": {
      "type": "number"
    },
    "dialogCounter": {
      "type": "number"
    },
    "location": {
      "type": "array"
    },
    "literal": {
      "type": "string"
    },
    "literalLength": {
      "type": "number"
    },
    "type": {
      "type": "string"
    }
  },
  "required":["name","confidence","dialogCounter","literalLength","type","literal","location"]
},
        "dialogNode":{

  "type": "object",
  "properties": {
    "name": {"type":"string"},
    "id": {"type":"number"},
    "description": {"type":"number"}
  },
    "required":["name","id","description"]
},
        "relations":{

  "type": "object"

},
        "entites":{
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "confidence": {
      "type": "number"
    },
    "dialogCounter": {
      "type": "number"
    },
    "location": {
      "type": "array"
    },
    "literal": {
      "type": "string"
    },
    "literalLength": {
      "type": "number"
    },
    "type": {
      "type": "string"
    }
  },
  "required":["name","confidence","dialogCounter","literalLength","type","literal","location"]
},
        "topIntentSchema":{
            "type":"object",
            "properties":{
                "name":{"type":"string"},
                "dialogCounter":{
                    "type":"array",
                    "contains":{
                        "allOf":[{"type":"number"}]
                    },
                    "minItems":0,
                    "items":{"type":"number"}
                    },
                "confidence":{
                    "type":"array",
                    "contains":{
                        "allOf":[{"type":"number"}]
                    },
                    "minItems":0,
                    "items":{"type":"number"}
                    },
                "lastLocDiff": {"type":"number"},
                "numOccurrence": {"type":"number"}
            }

    }}
  }


