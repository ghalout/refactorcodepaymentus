"""

self.intents=intents
        self.entities=entities
        self.relations=relations
        self.inputMessage=inputMessage
        self.outputMessages = outputMessages
        self.context=context
        self.botRequest= botRequest

"""
from schema_botRequest import schema as botRequestSchema
from schema_message    import schema as messageSchema
from schema_relations  import schema as relationSchema
from schema_entities   import schema as entitySchema
from schema_intents    import schema as intentSchema
from schema_context    import schema as contextSchema

schema={
      "type": "object",
      "properties": {
      "conversationId": {"type":"string"},
      "intents":{
       "type":"array",
        "contains":{"allOf":[{"$ref": "#/definitions/intent"}]},
        "minItems":0,
        "items":{"$ref": "#/definitions/intent"}

    },
    "entities":{
       "type":"array",
        "contains":{"allOf":[{"$ref": "#/definitions/entity"}]},
        "minItems":0,
        "items":{"$ref": "#/definitions/entity"}

    },
    "botRequest":botRequestSchema,
    "inputMessage":messageSchema,
    "outputMessages":{

        "type":"array",
        "contains":{"allOf":[{"$ref": "#/definitions/message"}]},
        "minItems":0,
        "items":{"$ref": "#/definitions/message"}

    },
    "context":contextSchema,
    "definitions":{
      "intent":intentSchema,
      "entity":entitySchema,
      "message":messageSchema,
      "relation":relationSchema,
      "botRequest":botRequestSchema

    }
    }
    }