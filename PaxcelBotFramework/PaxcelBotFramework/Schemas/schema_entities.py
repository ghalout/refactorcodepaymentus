schema = {
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "confidence": {
      "type": "number"
    },
    "dialogCounter": {
      "type": "number"
    },
    "location": {
      "type": "array"
    },
    "literal": {
      "type": "string"
    },
    "literalLength": {
      "type": "number"
    },
    "type": {
      "type": "string"
    }
  },
  "required":["name","confidence","dialogCounter","literalLength","type","literal","location"]
}
