schema = {

  "type": "object",
  "properties": {
    "name": {"type":"string"},
    "confidence": {"type":"number"},
    "dialogCounter": {"type":"number"}
  },
  "required":["name","confidence","dialogCounter"]
}