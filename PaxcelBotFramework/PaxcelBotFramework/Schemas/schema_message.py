schema = {
  "type": "object",
  "properties": {
    "message": {"type":"string"},
    "type": {"type":"string"},
    "entitesSubstituted": {"type":"string"},
    "dialogCounter": {"type":"number"},
    "raw":{"type":"object"}
  },
  "required":["message","type","raw"] #"entitesSubstituted","dialogCounter",
}




