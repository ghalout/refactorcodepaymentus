from schema_context import schema as contextSchema
schema = {
  "type": "object",
  "properties": {
    "info":{"type":"object"},
    "context":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/context"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/context"}
        },
    "SimpleMessage":
        {
            "type":"array",
            "contains":{
                        "allOf":[{"type":"string"}]
            },
            "minItems":0,
            "items":{"type":"string"}
        },
    "componentTypes":{
            "type":"array",
            "contains":{
                        "allOf":[{"type":"string"}]
            },
            "minItems":0,
            "items":{"type":"string"}

    }
  },
  "definitions":{
      "context":contextSchema
  },
   "required":["info","context","componentTypes"]
}


