masterSchema={
  "intents": {
    "required": [
      "name",
      "confidence",
      "dialogCounter"
    ],
    "type": "object",
    "properties": {
      "dialogCounter": {
        "type": "number"
      },
      "confidence": {
        "type": "number"
      },
      "name": {
        "type": "string"
      }
    }
  },
  "Feedback": {
    "type": "object",
    "properties": {
      "info": {
        "type": "object"
      },
      "componentType": {
        "patternProperties": {
          "progBinaryName": "FeedBack"
        },
        "type": "string"
      },
      "message": {
        "type": "string"
      },
      "tag": {
        "type": "string"
      },
      "name": {
        "type": "string"
      }
    }
  },
  "botResponse": {
    "type": "object",
    "properties": {
      "conversationId": {
        "type": "string"
      },
      "entities": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/entity"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/entity"
            }
          ]
        },
        "type": "array"
      },
      "intents": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/intent"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/intent"
            }
          ]
        },
        "type": "array"
      },
      "botRequest": {
        "definitions": {
          "topIntentSchema": {
            "type": "object",
            "properties": {
              "dialogCounter": {
                "minItems": 0,
                "items": {
                  "type": "number"
                },
                "contains": {
                  "allOf": [
                    {
                      "type": "number"
                    }
                  ]
                },
                "type": "array"
              },
              "confidence": {
                "minItems": 0,
                "items": {
                  "type": "number"
                },
                "contains": {
                  "allOf": [
                    {
                      "type": "number"
                    }
                  ]
                },
                "type": "array"
              },
              "lastLocDiff": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "numOccurrence": {
                "type": "number"
              }
            }
          },
          "relations": {
            "type": "object"
          },
          "relation": {
            "type": "object"
          },
          "messageSchema": {
            "required": [
              "message",
              "type",
              "raw"
            ],
            "type": "object",
            "properties": {
              "dialogCounter": {
                "type": "number"
              },
              "message": {
                "type": "string"
              },
              "type": {
                "type": "string"
              },
              "entitesSubstituted": {
                "type": "string"
              },
              "raw": {
                "type": "object"
              }
            }
          },
          "context": {
            "type": "object",
            "properties": {
              "conversationId": {
                "type": "string"
              },
              "contextElements": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/contextElement"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/contextElement"
                    }
                  ]
                },
                "type": "array"
              },
              "topIntents": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/topIntentSchema"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/topIntentSchema"
                    }
                  ]
                },
                "type": "array"
              },
              "entities": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/entity"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/entity"
                    }
                  ]
                },
                "type": "array"
              }
            }
          },
          "contextElement": {
            "required": [
              "conversationId",
              "intents",
              "entities",
              "nodesVisited",
              "raw",
              "dialogCounter"
            ],
            "type": "object",
            "properties": {
              "raw": {
                "type": "object"
              },
              "intents": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/intent"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/intent"
                    }
                  ]
                },
                "type": "array"
              },
              "dialogCounter": {
                "type": "number"
              },
              "nodesVisited": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/dialogNode"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/dialogNode"
                    }
                  ]
                },
                "type": "array"
              },
              "conversationId": {
                "type": "string"
              },
              "entities": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/entity"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/entity"
                    }
                  ]
                },
                "type": "array"
              }
            }
          },
          "entites": {
            "required": [
              "name",
              "confidence",
              "dialogCounter",
              "literalLength",
              "type",
              "literal",
              "location"
            ],
            "type": "object",
            "properties": {
              "confidence": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "literalLength": {
                "type": "number"
              },
              "dialogCounter": {
                "type": "number"
              },
              "literal": {
                "type": "string"
              },
              "location": {
                "type": "array"
              },
              "type": {
                "type": "string"
              }
            }
          },
          "intent": {
            "required": [
              "name",
              "confidence",
              "dialogCounter"
            ],
            "type": "object",
            "properties": {
              "dialogCounter": {
                "type": "number"
              },
              "confidence": {
                "type": "number"
              },
              "name": {
                "type": "string"
              }
            }
          },
          "dialogNode": {
            "required": [
              "name",
              "id",
              "description"
            ],
            "type": "object",
            "properties": {
              "description": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "id": {
                "type": "number"
              }
            }
          },
          "entity": {
            "required": [
              "name",
              "confidence",
              "dialogCounter",
              "literalLength",
              "type",
              "literal",
              "location"
            ],
            "type": "object",
            "properties": {
              "confidence": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "literalLength": {
                "type": "number"
              },
              "dialogCounter": {
                "type": "number"
              },
              "literal": {
                "type": "string"
              },
              "location": {
                "type": "array"
              },
              "type": {
                "type": "string"
              }
            }
          }
        },
        "type": "object",
        "properties": {
          "info": {
            "type": "object"
          },
          "context": {
            "$ref": "#/definitions/context"
          },
          "templateBotTag": {
            "type": "string"
          },
          "rawInput": {
            "type": "object"
          },
          "message": {
            "$ref": "#/definitions/messageSchema"
          },
          "computeRelations": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/relation"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/relation"
                }
              ]
            },
            "type": "array"
          }
        }
      },
      "context": {
        "definitions": {
          "entites": {
            "required": [
              "name",
              "confidence",
              "dialogCounter",
              "literalLength",
              "type",
              "literal",
              "location"
            ],
            "type": "object",
            "properties": {
              "confidence": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "literalLength": {
                "type": "number"
              },
              "dialogCounter": {
                "type": "number"
              },
              "literal": {
                "type": "string"
              },
              "location": {
                "type": "array"
              },
              "type": {
                "type": "string"
              }
            }
          },
          "relations": {
            "type": "object"
          },
          "entity": {
            "required": [
              "name",
              "confidence",
              "dialogCounter",
              "literalLength",
              "type",
              "literal",
              "location"
            ],
            "type": "object",
            "properties": {
              "confidence": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "literalLength": {
                "type": "number"
              },
              "dialogCounter": {
                "type": "number"
              },
              "literal": {
                "type": "string"
              },
              "location": {
                "type": "array"
              },
              "type": {
                "type": "string"
              }
            }
          },
          "topIntentSchema": {
            "type": "object",
            "properties": {
              "dialogCounter": {
                "minItems": 0,
                "items": {
                  "type": "number"
                },
                "contains": {
                  "allOf": [
                    {
                      "type": "number"
                    }
                  ]
                },
                "type": "array"
              },
              "confidence": {
                "minItems": 0,
                "items": {
                  "type": "number"
                },
                "contains": {
                  "allOf": [
                    {
                      "type": "number"
                    }
                  ]
                },
                "type": "array"
              },
              "lastLocDiff": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "numOccurrence": {
                "type": "number"
              }
            }
          },
          "intent": {
            "required": [
              "name",
              "confidence",
              "dialogCounter"
            ],
            "type": "object",
            "properties": {
              "dialogCounter": {
                "type": "number"
              },
              "confidence": {
                "type": "number"
              },
              "name": {
                "type": "string"
              }
            }
          },
          "contextElement": {
            "required": [
              "conversationId",
              "intents",
              "entities",
              "nodesVisited",
              "raw",
              "dialogCounter"
            ],
            "type": "object",
            "properties": {
              "raw": {
                "type": "object"
              },
              "intents": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/intent"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/intent"
                    }
                  ]
                },
                "type": "array"
              },
              "dialogCounter": {
                "type": "number"
              },
              "nodesVisited": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/dialogNode"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/dialogNode"
                    }
                  ]
                },
                "type": "array"
              },
              "conversationId": {
                "type": "string"
              },
              "entities": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/entity"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/entity"
                    }
                  ]
                },
                "type": "array"
              }
            }
          },
          "dialogNode": {
            "required": [
              "name",
              "id",
              "description"
            ],
            "type": "object",
            "properties": {
              "description": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "id": {
                "type": "number"
              }
            }
          }
        },
        "type": "object",
        "properties": {
          "conversationId": {
            "type": "string"
          },
          "contextElements": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/contextElement"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/contextElement"
                }
              ]
            },
            "type": "array"
          },
          "topIntents": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/topIntentSchema"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/topIntentSchema"
                }
              ]
            },
            "type": "array"
          },
          "entities": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/entity"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/entity"
                }
              ]
            },
            "type": "array"
          }
        }
      },
      "outputMessages": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/message"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/message"
            }
          ]
        },
        "type": "array"
      },
      "definitions": {
        "relation": {
          "type": "object"
        },
        "message": {
          "required": [
            "message",
            "type",
            "raw"
          ],
          "type": "object",
          "properties": {
            "dialogCounter": {
              "type": "number"
            },
            "message": {
              "type": "string"
            },
            "type": {
              "type": "string"
            },
            "entitesSubstituted": {
              "type": "string"
            },
            "raw": {
              "type": "object"
            }
          }
        },
        "intent": {
          "required": [
            "name",
            "confidence",
            "dialogCounter"
          ],
          "type": "object",
          "properties": {
            "dialogCounter": {
              "type": "number"
            },
            "confidence": {
              "type": "number"
            },
            "name": {
              "type": "string"
            }
          }
        },
        "botRequest": {
          "definitions": {
            "topIntentSchema": {
              "type": "object",
              "properties": {
                "dialogCounter": {
                  "minItems": 0,
                  "items": {
                    "type": "number"
                  },
                  "contains": {
                    "allOf": [
                      {
                        "type": "number"
                      }
                    ]
                  },
                  "type": "array"
                },
                "confidence": {
                  "minItems": 0,
                  "items": {
                    "type": "number"
                  },
                  "contains": {
                    "allOf": [
                      {
                        "type": "number"
                      }
                    ]
                  },
                  "type": "array"
                },
                "lastLocDiff": {
                  "type": "number"
                },
                "name": {
                  "type": "string"
                },
                "numOccurrence": {
                  "type": "number"
                }
              }
            },
            "relations": {
              "type": "object"
            },
            "relation": {
              "type": "object"
            },
            "messageSchema": {
              "required": [
                "message",
                "type",
                "raw"
              ],
              "type": "object",
              "properties": {
                "dialogCounter": {
                  "type": "number"
                },
                "message": {
                  "type": "string"
                },
                "type": {
                  "type": "string"
                },
                "entitesSubstituted": {
                  "type": "string"
                },
                "raw": {
                  "type": "object"
                }
              }
            },
            "context": {
              "type": "object",
              "properties": {
                "conversationId": {
                  "type": "string"
                },
                "contextElements": {
                  "minItems": 0,
                  "items": {
                    "$ref": "#/definitions/contextElement"
                  },
                  "contains": {
                    "allOf": [
                      {
                        "$ref": "#/definitions/contextElement"
                      }
                    ]
                  },
                  "type": "array"
                },
                "topIntents": {
                  "minItems": 0,
                  "items": {
                    "$ref": "#/definitions/topIntentSchema"
                  },
                  "contains": {
                    "allOf": [
                      {
                        "$ref": "#/definitions/topIntentSchema"
                      }
                    ]
                  },
                  "type": "array"
                },
                "entities": {
                  "minItems": 0,
                  "items": {
                    "$ref": "#/definitions/entity"
                  },
                  "contains": {
                    "allOf": [
                      {
                        "$ref": "#/definitions/entity"
                      }
                    ]
                  },
                  "type": "array"
                }
              }
            },
            "contextElement": {
              "required": [
                "conversationId",
                "intents",
                "entities",
                "nodesVisited",
                "raw",
                "dialogCounter"
              ],
              "type": "object",
              "properties": {
                "raw": {
                  "type": "object"
                },
                "intents": {
                  "minItems": 0,
                  "items": {
                    "$ref": "#/definitions/intent"
                  },
                  "contains": {
                    "allOf": [
                      {
                        "$ref": "#/definitions/intent"
                      }
                    ]
                  },
                  "type": "array"
                },
                "dialogCounter": {
                  "type": "number"
                },
                "nodesVisited": {
                  "minItems": 0,
                  "items": {
                    "$ref": "#/definitions/dialogNode"
                  },
                  "contains": {
                    "allOf": [
                      {
                        "$ref": "#/definitions/dialogNode"
                      }
                    ]
                  },
                  "type": "array"
                },
                "conversationId": {
                  "type": "string"
                },
                "entities": {
                  "minItems": 0,
                  "items": {
                    "$ref": "#/definitions/entity"
                  },
                  "contains": {
                    "allOf": [
                      {
                        "$ref": "#/definitions/entity"
                      }
                    ]
                  },
                  "type": "array"
                }
              }
            },
            "entites": {
              "required": [
                "name",
                "confidence",
                "dialogCounter",
                "literalLength",
                "type",
                "literal",
                "location"
              ],
              "type": "object",
              "properties": {
                "confidence": {
                  "type": "number"
                },
                "name": {
                  "type": "string"
                },
                "literalLength": {
                  "type": "number"
                },
                "dialogCounter": {
                  "type": "number"
                },
                "literal": {
                  "type": "string"
                },
                "location": {
                  "type": "array"
                },
                "type": {
                  "type": "string"
                }
              }
            },
            "intent": {
              "required": [
                "name",
                "confidence",
                "dialogCounter"
              ],
              "type": "object",
              "properties": {
                "dialogCounter": {
                  "type": "number"
                },
                "confidence": {
                  "type": "number"
                },
                "name": {
                  "type": "string"
                }
              }
            },
            "dialogNode": {
              "required": [
                "name",
                "id",
                "description"
              ],
              "type": "object",
              "properties": {
                "description": {
                  "type": "number"
                },
                "name": {
                  "type": "string"
                },
                "id": {
                  "type": "number"
                }
              }
            },
            "entity": {
              "required": [
                "name",
                "confidence",
                "dialogCounter",
                "literalLength",
                "type",
                "literal",
                "location"
              ],
              "type": "object",
              "properties": {
                "confidence": {
                  "type": "number"
                },
                "name": {
                  "type": "string"
                },
                "literalLength": {
                  "type": "number"
                },
                "dialogCounter": {
                  "type": "number"
                },
                "literal": {
                  "type": "string"
                },
                "location": {
                  "type": "array"
                },
                "type": {
                  "type": "string"
                }
              }
            }
          },
          "type": "object",
          "properties": {
            "info": {
              "type": "object"
            },
            "context": {
              "$ref": "#/definitions/context"
            },
            "templateBotTag": {
              "type": "string"
            },
            "rawInput": {
              "type": "object"
            },
            "message": {
              "$ref": "#/definitions/messageSchema"
            },
            "computeRelations": {
              "minItems": 0,
              "items": {
                "$ref": "#/definitions/relation"
              },
              "contains": {
                "allOf": [
                  {
                    "$ref": "#/definitions/relation"
                  }
                ]
              },
              "type": "array"
            }
          }
        },
        "entity": {
          "required": [
            "name",
            "confidence",
            "dialogCounter",
            "literalLength",
            "type",
            "literal",
            "location"
          ],
          "type": "object",
          "properties": {
            "confidence": {
              "type": "number"
            },
            "name": {
              "type": "string"
            },
            "literalLength": {
              "type": "number"
            },
            "dialogCounter": {
              "type": "number"
            },
            "literal": {
              "type": "string"
            },
            "location": {
              "type": "array"
            },
            "type": {
              "type": "string"
            }
          }
        }
      },
      "inputMessage": {
        "required": [
          "message",
          "type",
          "raw"
        ],
        "type": "object",
        "properties": {
          "dialogCounter": {
            "type": "number"
          },
          "message": {
            "type": "string"
          },
          "type": {
            "type": "string"
          },
          "entitesSubstituted": {
            "type": "string"
          },
          "raw": {
            "type": "object"
          }
        }
      }
    }
  },
  "outputProcessor1": {
    "definitions": {
      "context": {
        "definitions": {
          "entites": {
            "required": [
              "name",
              "confidence",
              "dialogCounter",
              "literalLength",
              "type",
              "literal",
              "location"
            ],
            "type": "object",
            "properties": {
              "confidence": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "literalLength": {
                "type": "number"
              },
              "dialogCounter": {
                "type": "number"
              },
              "literal": {
                "type": "string"
              },
              "location": {
                "type": "array"
              },
              "type": {
                "type": "string"
              }
            }
          },
          "relations": {
            "type": "object"
          },
          "entity": {
            "required": [
              "name",
              "confidence",
              "dialogCounter",
              "literalLength",
              "type",
              "literal",
              "location"
            ],
            "type": "object",
            "properties": {
              "confidence": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "literalLength": {
                "type": "number"
              },
              "dialogCounter": {
                "type": "number"
              },
              "literal": {
                "type": "string"
              },
              "location": {
                "type": "array"
              },
              "type": {
                "type": "string"
              }
            }
          },
          "topIntentSchema": {
            "type": "object",
            "properties": {
              "dialogCounter": {
                "minItems": 0,
                "items": {
                  "type": "number"
                },
                "contains": {
                  "allOf": [
                    {
                      "type": "number"
                    }
                  ]
                },
                "type": "array"
              },
              "confidence": {
                "minItems": 0,
                "items": {
                  "type": "number"
                },
                "contains": {
                  "allOf": [
                    {
                      "type": "number"
                    }
                  ]
                },
                "type": "array"
              },
              "lastLocDiff": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "numOccurrence": {
                "type": "number"
              }
            }
          },
          "intent": {
            "required": [
              "name",
              "confidence",
              "dialogCounter"
            ],
            "type": "object",
            "properties": {
              "dialogCounter": {
                "type": "number"
              },
              "confidence": {
                "type": "number"
              },
              "name": {
                "type": "string"
              }
            }
          },
          "contextElement": {
            "required": [
              "conversationId",
              "intents",
              "entities",
              "nodesVisited",
              "raw",
              "dialogCounter"
            ],
            "type": "object",
            "properties": {
              "raw": {
                "type": "object"
              },
              "intents": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/intent"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/intent"
                    }
                  ]
                },
                "type": "array"
              },
              "dialogCounter": {
                "type": "number"
              },
              "nodesVisited": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/dialogNode"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/dialogNode"
                    }
                  ]
                },
                "type": "array"
              },
              "conversationId": {
                "type": "string"
              },
              "entities": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/entity"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/entity"
                    }
                  ]
                },
                "type": "array"
              }
            }
          },
          "dialogNode": {
            "required": [
              "name",
              "id",
              "description"
            ],
            "type": "object",
            "properties": {
              "description": {
                "type": "number"
              },
              "name": {
                "type": "string"
              },
              "id": {
                "type": "number"
              }
            }
          }
        },
        "type": "object",
        "properties": {
          "conversationId": {
            "type": "string"
          },
          "contextElements": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/contextElement"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/contextElement"
                }
              ]
            },
            "type": "array"
          },
          "topIntents": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/topIntentSchema"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/topIntentSchema"
                }
              ]
            },
            "type": "array"
          },
          "entities": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/entity"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/entity"
                }
              ]
            },
            "type": "array"
          }
        }
      }
    },
    "required": [
      "info",
      "context",
      "componentTypes"
    ],
    "type": "object",
    "properties": {
      "info": {
        "type": "object"
      },
      "componentTypes": {
        "minItems": 0,
        "items": {
          "type": "string"
        },
        "contains": {
          "allOf": [
            {
              "type": "string"
            }
          ]
        },
        "type": "array"
      },
      "SimpleMessage": {
        "minItems": 0,
        "items": {
          "type": "string"
        },
        "contains": {
          "allOf": [
            {
              "type": "string"
            }
          ]
        },
        "type": "array"
      },
      "context": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/context"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/context"
            }
          ]
        },
        "type": "array"
      }
    }
  },
  "AuthForm": {
    "type": "object",
    "properties": {
      "info": {
        "type": "object"
      },
      "componentType": {
        "patternProperties": {
          "progBinaryName": "AuthForm"
        },
        "type": "string"
      },
      "message": {
        "type": "string"
      },
      "tag": {
        "type": "string"
      },
      "name": {
        "type": "string"
      }
    }
  },
  "entities": {
    "required": [
      "name",
      "confidence",
      "dialogCounter",
      "literalLength",
      "type",
      "literal",
      "location"
    ],
    "type": "object",
    "properties": {
      "confidence": {
        "type": "number"
      },
      "name": {
        "type": "string"
      },
      "literalLength": {
        "type": "number"
      },
      "dialogCounter": {
        "type": "number"
      },
      "literal": {
        "type": "string"
      },
      "location": {
        "type": "array"
      },
      "type": {
        "type": "string"
      }
    }
  },
  "SimpleMessage": {
    "type": "object",
    "properties": {
      "componentType": {
        "patternProperties": {
          "progBinaryName": "SimpleMessage"
        },
        "type": "string"
      },
      "message": {
        "type": "string"
      },
      "tag": {
        "type": "string"
      },
      "data": {
        "type": "object"
      },
      "name": {
        "type": "string"
      }
    }
  },
  "botRequest": {
    "definitions": {
      "topIntentSchema": {
        "type": "object",
        "properties": {
          "dialogCounter": {
            "minItems": 0,
            "items": {
              "type": "number"
            },
            "contains": {
              "allOf": [
                {
                  "type": "number"
                }
              ]
            },
            "type": "array"
          },
          "confidence": {
            "minItems": 0,
            "items": {
              "type": "number"
            },
            "contains": {
              "allOf": [
                {
                  "type": "number"
                }
              ]
            },
            "type": "array"
          },
          "lastLocDiff": {
            "type": "number"
          },
          "name": {
            "type": "string"
          },
          "numOccurrence": {
            "type": "number"
          }
        }
      },
      "relations": {
        "type": "object"
      },
      "relation": {
        "type": "object"
      },
      "messageSchema": {
        "required": [
          "message",
          "type",
          "raw"
        ],
        "type": "object",
        "properties": {
          "dialogCounter": {
            "type": "number"
          },
          "message": {
            "type": "string"
          },
          "type": {
            "type": "string"
          },
          "entitesSubstituted": {
            "type": "string"
          },
          "raw": {
            "type": "object"
          }
        }
      },
      "context": {
        "type": "object",
        "properties": {
          "conversationId": {
            "type": "string"
          },
          "contextElements": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/contextElement"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/contextElement"
                }
              ]
            },
            "type": "array"
          },
          "topIntents": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/topIntentSchema"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/topIntentSchema"
                }
              ]
            },
            "type": "array"
          },
          "entities": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/entity"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/entity"
                }
              ]
            },
            "type": "array"
          }
        }
      },
      "contextElement": {
        "required": [
          "conversationId",
          "intents",
          "entities",
          "nodesVisited",
          "raw",
          "dialogCounter"
        ],
        "type": "object",
        "properties": {
          "raw": {
            "type": "object"
          },
          "intents": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/intent"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/intent"
                }
              ]
            },
            "type": "array"
          },
          "dialogCounter": {
            "type": "number"
          },
          "nodesVisited": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/dialogNode"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/dialogNode"
                }
              ]
            },
            "type": "array"
          },
          "conversationId": {
            "type": "string"
          },
          "entities": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/entity"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/entity"
                }
              ]
            },
            "type": "array"
          }
        }
      },
      "entites": {
        "required": [
          "name",
          "confidence",
          "dialogCounter",
          "literalLength",
          "type",
          "literal",
          "location"
        ],
        "type": "object",
        "properties": {
          "confidence": {
            "type": "number"
          },
          "name": {
            "type": "string"
          },
          "literalLength": {
            "type": "number"
          },
          "dialogCounter": {
            "type": "number"
          },
          "literal": {
            "type": "string"
          },
          "location": {
            "type": "array"
          },
          "type": {
            "type": "string"
          }
        }
      },
      "intent": {
        "required": [
          "name",
          "confidence",
          "dialogCounter"
        ],
        "type": "object",
        "properties": {
          "dialogCounter": {
            "type": "number"
          },
          "confidence": {
            "type": "number"
          },
          "name": {
            "type": "string"
          }
        }
      },
      "dialogNode": {
        "required": [
          "name",
          "id",
          "description"
        ],
        "type": "object",
        "properties": {
          "description": {
            "type": "number"
          },
          "name": {
            "type": "string"
          },
          "id": {
            "type": "number"
          }
        }
      },
      "entity": {
        "required": [
          "name",
          "confidence",
          "dialogCounter",
          "literalLength",
          "type",
          "literal",
          "location"
        ],
        "type": "object",
        "properties": {
          "confidence": {
            "type": "number"
          },
          "name": {
            "type": "string"
          },
          "literalLength": {
            "type": "number"
          },
          "dialogCounter": {
            "type": "number"
          },
          "literal": {
            "type": "string"
          },
          "location": {
            "type": "array"
          },
          "type": {
            "type": "string"
          }
        }
      }
    },
    "type": "object",
    "properties": {
      "info": {
        "type": "object"
      },
      "context": {
        "$ref": "#/definitions/context"
      },
      "templateBotTag": {
        "type": "string"
      },
      "rawInput": {
        "type": "object"
      },
      "message": {
        "$ref": "#/definitions/messageSchema"
      },
      "computeRelations": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/relation"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/relation"
            }
          ]
        },
        "type": "array"
      }
    }
  },
  "relations": {
    "type": "object"
  },
  "context": {
    "definitions": {
      "entites": {
        "required": [
          "name",
          "confidence",
          "dialogCounter",
          "literalLength",
          "type",
          "literal",
          "location"
        ],
        "type": "object",
        "properties": {
          "confidence": {
            "type": "number"
          },
          "name": {
            "type": "string"
          },
          "literalLength": {
            "type": "number"
          },
          "dialogCounter": {
            "type": "number"
          },
          "literal": {
            "type": "string"
          },
          "location": {
            "type": "array"
          },
          "type": {
            "type": "string"
          }
        }
      },
      "relations": {
        "type": "object"
      },
      "entity": {
        "required": [
          "name",
          "confidence",
          "dialogCounter",
          "literalLength",
          "type",
          "literal",
          "location"
        ],
        "type": "object",
        "properties": {
          "confidence": {
            "type": "number"
          },
          "name": {
            "type": "string"
          },
          "literalLength": {
            "type": "number"
          },
          "dialogCounter": {
            "type": "number"
          },
          "literal": {
            "type": "string"
          },
          "location": {
            "type": "array"
          },
          "type": {
            "type": "string"
          }
        }
      },
      "topIntentSchema": {
        "type": "object",
        "properties": {
          "dialogCounter": {
            "minItems": 0,
            "items": {
              "type": "number"
            },
            "contains": {
              "allOf": [
                {
                  "type": "number"
                }
              ]
            },
            "type": "array"
          },
          "confidence": {
            "minItems": 0,
            "items": {
              "type": "number"
            },
            "contains": {
              "allOf": [
                {
                  "type": "number"
                }
              ]
            },
            "type": "array"
          },
          "lastLocDiff": {
            "type": "number"
          },
          "name": {
            "type": "string"
          },
          "numOccurrence": {
            "type": "number"
          }
        }
      },
      "intent": {
        "required": [
          "name",
          "confidence",
          "dialogCounter"
        ],
        "type": "object",
        "properties": {
          "dialogCounter": {
            "type": "number"
          },
          "confidence": {
            "type": "number"
          },
          "name": {
            "type": "string"
          }
        }
      },
      "contextElement": {
        "required": [
          "conversationId",
          "intents",
          "entities",
          "nodesVisited",
          "raw",
          "dialogCounter"
        ],
        "type": "object",
        "properties": {
          "raw": {
            "type": "object"
          },
          "intents": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/intent"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/intent"
                }
              ]
            },
            "type": "array"
          },
          "dialogCounter": {
            "type": "number"
          },
          "nodesVisited": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/dialogNode"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/dialogNode"
                }
              ]
            },
            "type": "array"
          },
          "conversationId": {
            "type": "string"
          },
          "entities": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/entity"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/entity"
                }
              ]
            },
            "type": "array"
          }
        }
      },
      "dialogNode": {
        "required": [
          "name",
          "id",
          "description"
        ],
        "type": "object",
        "properties": {
          "description": {
            "type": "number"
          },
          "name": {
            "type": "string"
          },
          "id": {
            "type": "number"
          }
        }
      }
    },
    "type": "object",
    "properties": {
      "conversationId": {
        "type": "string"
      },
      "contextElements": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/contextElement"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/contextElement"
            }
          ]
        },
        "type": "array"
      },
      "topIntents": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/topIntentSchema"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/topIntentSchema"
            }
          ]
        },
        "type": "array"
      },
      "entities": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/entity"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/entity"
            }
          ]
        },
        "type": "array"
      }
    }
  },
  "UIComponentSchema": {
    "type": "object",
    "properties": {
      "tag": {
        "type": "string"
      }
    }
  },
  "contextElement": {
    "definitions": {
      "intent": {
        "required": [
          "name",
          "confidence",
          "dialogCounter"
        ],
        "type": "object",
        "properties": {
          "dialogCounter": {
            "type": "number"
          },
          "confidence": {
            "type": "number"
          },
          "name": {
            "type": "string"
          }
        }
      },
      "relations": {
        "type": "object"
      },
      "dialogNode": {
        "required": [
          "name",
          "id",
          "description"
        ],
        "type": "object",
        "properties": {
          "description": {
            "type": "number"
          },
          "name": {
            "type": "string"
          },
          "id": {
            "type": "number"
          }
        }
      },
      "entity": {
        "required": [
          "name",
          "confidence",
          "dialogCounter",
          "literalLength",
          "type",
          "literal",
          "location"
        ],
        "type": "object",
        "properties": {
          "confidence": {
            "type": "number"
          },
          "name": {
            "type": "string"
          },
          "literalLength": {
            "type": "number"
          },
          "dialogCounter": {
            "type": "number"
          },
          "literal": {
            "type": "string"
          },
          "location": {
            "type": "array"
          },
          "type": {
            "type": "string"
          }
        }
      }
    },
    "required": [
      "conversationId",
      "intents",
      "entities",
      "nodesVisited",
      "raw",
      "dialogCounter"
    ],
    "type": "object",
    "properties": {
      "raw": {
        "type": "object"
      },
      "intents": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/intent"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/intent"
            }
          ]
        },
        "type": "array"
      },
      "dialogCounter": {
        "type": "number"
      },
      "nodesVisited": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/dialogNode"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/dialogNode"
            }
          ]
        },
        "type": "array"
      },
      "conversationId": {
        "type": "string"
      },
      "entities": {
        "minItems": 0,
        "items": {
          "$ref": "#/definitions/entity"
        },
        "contains": {
          "allOf": [
            {
              "$ref": "#/definitions/entity"
            }
          ]
        },
        "type": "array"
      }
    }
  },
  "message": {
    "required": [
      "message",
      "type",
      "raw"
    ],
    "type": "object",
    "properties": {
      "dialogCounter": {
        "type": "number"
      },
      "message": {
        "type": "string"
      },
      "type": {
        "type": "string"
      },
      "entitesSubstituted": {
        "type": "string"
      },
      "raw": {
        "type": "object"
      }
    }
  },
  "outputContainer": {
    "definitions": {
      "Feedback": {
        "type": "object",
        "properties": {
          "info": {
            "type": "object"
          },
          "componentType": {
            "patternProperties": {
              "progBinaryName": "FeedBack"
            },
            "type": "string"
          },
          "message": {
            "type": "string"
          },
          "tag": {
            "type": "string"
          },
          "name": {
            "type": "string"
          }
        }
      },
      "botResponse": {
        "type": "object",
        "properties": {
          "conversationId": {
            "type": "string"
          },
          "entities": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/entity"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/entity"
                }
              ]
            },
            "type": "array"
          },
          "intents": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/intent"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/intent"
                }
              ]
            },
            "type": "array"
          },
          "botRequest": {
            "definitions": {
              "topIntentSchema": {
                "type": "object",
                "properties": {
                  "dialogCounter": {
                    "minItems": 0,
                    "items": {
                      "type": "number"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "type": "number"
                        }
                      ]
                    },
                    "type": "array"
                  },
                  "confidence": {
                    "minItems": 0,
                    "items": {
                      "type": "number"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "type": "number"
                        }
                      ]
                    },
                    "type": "array"
                  },
                  "lastLocDiff": {
                    "type": "number"
                  },
                  "name": {
                    "type": "string"
                  },
                  "numOccurrence": {
                    "type": "number"
                  }
                }
              },
              "relations": {
                "type": "object"
              },
              "relation": {
                "type": "object"
              },
              "messageSchema": {
                "required": [
                  "message",
                  "type",
                  "raw"
                ],
                "type": "object",
                "properties": {
                  "dialogCounter": {
                    "type": "number"
                  },
                  "message": {
                    "type": "string"
                  },
                  "type": {
                    "type": "string"
                  },
                  "entitesSubstituted": {
                    "type": "string"
                  },
                  "raw": {
                    "type": "object"
                  }
                }
              },
              "context": {
                "type": "object",
                "properties": {
                  "conversationId": {
                    "type": "string"
                  },
                  "contextElements": {
                    "minItems": 0,
                    "items": {
                      "$ref": "#/definitions/contextElement"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "$ref": "#/definitions/contextElement"
                        }
                      ]
                    },
                    "type": "array"
                  },
                  "topIntents": {
                    "minItems": 0,
                    "items": {
                      "$ref": "#/definitions/topIntentSchema"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "$ref": "#/definitions/topIntentSchema"
                        }
                      ]
                    },
                    "type": "array"
                  },
                  "entities": {
                    "minItems": 0,
                    "items": {
                      "$ref": "#/definitions/entity"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "$ref": "#/definitions/entity"
                        }
                      ]
                    },
                    "type": "array"
                  }
                }
              },
              "contextElement": {
                "required": [
                  "conversationId",
                  "intents",
                  "entities",
                  "nodesVisited",
                  "raw",
                  "dialogCounter"
                ],
                "type": "object",
                "properties": {
                  "raw": {
                    "type": "object"
                  },
                  "intents": {
                    "minItems": 0,
                    "items": {
                      "$ref": "#/definitions/intent"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "$ref": "#/definitions/intent"
                        }
                      ]
                    },
                    "type": "array"
                  },
                  "dialogCounter": {
                    "type": "number"
                  },
                  "nodesVisited": {
                    "minItems": 0,
                    "items": {
                      "$ref": "#/definitions/dialogNode"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "$ref": "#/definitions/dialogNode"
                        }
                      ]
                    },
                    "type": "array"
                  },
                  "conversationId": {
                    "type": "string"
                  },
                  "entities": {
                    "minItems": 0,
                    "items": {
                      "$ref": "#/definitions/entity"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "$ref": "#/definitions/entity"
                        }
                      ]
                    },
                    "type": "array"
                  }
                }
              },
              "entites": {
                "required": [
                  "name",
                  "confidence",
                  "dialogCounter",
                  "literalLength",
                  "type",
                  "literal",
                  "location"
                ],
                "type": "object",
                "properties": {
                  "confidence": {
                    "type": "number"
                  },
                  "name": {
                    "type": "string"
                  },
                  "literalLength": {
                    "type": "number"
                  },
                  "dialogCounter": {
                    "type": "number"
                  },
                  "literal": {
                    "type": "string"
                  },
                  "location": {
                    "type": "array"
                  },
                  "type": {
                    "type": "string"
                  }
                }
              },
              "intent": {
                "required": [
                  "name",
                  "confidence",
                  "dialogCounter"
                ],
                "type": "object",
                "properties": {
                  "dialogCounter": {
                    "type": "number"
                  },
                  "confidence": {
                    "type": "number"
                  },
                  "name": {
                    "type": "string"
                  }
                }
              },
              "dialogNode": {
                "required": [
                  "name",
                  "id",
                  "description"
                ],
                "type": "object",
                "properties": {
                  "description": {
                    "type": "number"
                  },
                  "name": {
                    "type": "string"
                  },
                  "id": {
                    "type": "number"
                  }
                }
              },
              "entity": {
                "required": [
                  "name",
                  "confidence",
                  "dialogCounter",
                  "literalLength",
                  "type",
                  "literal",
                  "location"
                ],
                "type": "object",
                "properties": {
                  "confidence": {
                    "type": "number"
                  },
                  "name": {
                    "type": "string"
                  },
                  "literalLength": {
                    "type": "number"
                  },
                  "dialogCounter": {
                    "type": "number"
                  },
                  "literal": {
                    "type": "string"
                  },
                  "location": {
                    "type": "array"
                  },
                  "type": {
                    "type": "string"
                  }
                }
              }
            },
            "type": "object",
            "properties": {
              "info": {
                "type": "object"
              },
              "context": {
                "$ref": "#/definitions/context"
              },
              "templateBotTag": {
                "type": "string"
              },
              "rawInput": {
                "type": "object"
              },
              "message": {
                "$ref": "#/definitions/messageSchema"
              },
              "computeRelations": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/relation"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/relation"
                    }
                  ]
                },
                "type": "array"
              }
            }
          },
          "context": {
            "definitions": {
              "entites": {
                "required": [
                  "name",
                  "confidence",
                  "dialogCounter",
                  "literalLength",
                  "type",
                  "literal",
                  "location"
                ],
                "type": "object",
                "properties": {
                  "confidence": {
                    "type": "number"
                  },
                  "name": {
                    "type": "string"
                  },
                  "literalLength": {
                    "type": "number"
                  },
                  "dialogCounter": {
                    "type": "number"
                  },
                  "literal": {
                    "type": "string"
                  },
                  "location": {
                    "type": "array"
                  },
                  "type": {
                    "type": "string"
                  }
                }
              },
              "relations": {
                "type": "object"
              },
              "entity": {
                "required": [
                  "name",
                  "confidence",
                  "dialogCounter",
                  "literalLength",
                  "type",
                  "literal",
                  "location"
                ],
                "type": "object",
                "properties": {
                  "confidence": {
                    "type": "number"
                  },
                  "name": {
                    "type": "string"
                  },
                  "literalLength": {
                    "type": "number"
                  },
                  "dialogCounter": {
                    "type": "number"
                  },
                  "literal": {
                    "type": "string"
                  },
                  "location": {
                    "type": "array"
                  },
                  "type": {
                    "type": "string"
                  }
                }
              },
              "topIntentSchema": {
                "type": "object",
                "properties": {
                  "dialogCounter": {
                    "minItems": 0,
                    "items": {
                      "type": "number"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "type": "number"
                        }
                      ]
                    },
                    "type": "array"
                  },
                  "confidence": {
                    "minItems": 0,
                    "items": {
                      "type": "number"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "type": "number"
                        }
                      ]
                    },
                    "type": "array"
                  },
                  "lastLocDiff": {
                    "type": "number"
                  },
                  "name": {
                    "type": "string"
                  },
                  "numOccurrence": {
                    "type": "number"
                  }
                }
              },
              "intent": {
                "required": [
                  "name",
                  "confidence",
                  "dialogCounter"
                ],
                "type": "object",
                "properties": {
                  "dialogCounter": {
                    "type": "number"
                  },
                  "confidence": {
                    "type": "number"
                  },
                  "name": {
                    "type": "string"
                  }
                }
              },
              "contextElement": {
                "required": [
                  "conversationId",
                  "intents",
                  "entities",
                  "nodesVisited",
                  "raw",
                  "dialogCounter"
                ],
                "type": "object",
                "properties": {
                  "raw": {
                    "type": "object"
                  },
                  "intents": {
                    "minItems": 0,
                    "items": {
                      "$ref": "#/definitions/intent"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "$ref": "#/definitions/intent"
                        }
                      ]
                    },
                    "type": "array"
                  },
                  "dialogCounter": {
                    "type": "number"
                  },
                  "nodesVisited": {
                    "minItems": 0,
                    "items": {
                      "$ref": "#/definitions/dialogNode"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "$ref": "#/definitions/dialogNode"
                        }
                      ]
                    },
                    "type": "array"
                  },
                  "conversationId": {
                    "type": "string"
                  },
                  "entities": {
                    "minItems": 0,
                    "items": {
                      "$ref": "#/definitions/entity"
                    },
                    "contains": {
                      "allOf": [
                        {
                          "$ref": "#/definitions/entity"
                        }
                      ]
                    },
                    "type": "array"
                  }
                }
              },
              "dialogNode": {
                "required": [
                  "name",
                  "id",
                  "description"
                ],
                "type": "object",
                "properties": {
                  "description": {
                    "type": "number"
                  },
                  "name": {
                    "type": "string"
                  },
                  "id": {
                    "type": "number"
                  }
                }
              }
            },
            "type": "object",
            "properties": {
              "conversationId": {
                "type": "string"
              },
              "contextElements": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/contextElement"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/contextElement"
                    }
                  ]
                },
                "type": "array"
              },
              "topIntents": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/topIntentSchema"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/topIntentSchema"
                    }
                  ]
                },
                "type": "array"
              },
              "entities": {
                "minItems": 0,
                "items": {
                  "$ref": "#/definitions/entity"
                },
                "contains": {
                  "allOf": [
                    {
                      "$ref": "#/definitions/entity"
                    }
                  ]
                },
                "type": "array"
              }
            }
          },
          "outputMessages": {
            "minItems": 0,
            "items": {
              "$ref": "#/definitions/message"
            },
            "contains": {
              "allOf": [
                {
                  "$ref": "#/definitions/message"
                }
              ]
            },
            "type": "array"
          },
          "definitions": {
            "relation": {
              "type": "object"
            },
            "message": {
              "required": [
                "message",
                "type",
                "raw"
              ],
              "type": "object",
              "properties": {
                "dialogCounter": {
                  "type": "number"
                },
                "message": {
                  "type": "string"
                },
                "type": {
                  "type": "string"
                },
                "entitesSubstituted": {
                  "type": "string"
                },
                "raw": {
                  "type": "object"
                }
              }
            },
            "intent": {
              "required": [
                "name",
                "confidence",
                "dialogCounter"
              ],
              "type": "object",
              "properties": {
                "dialogCounter": {
                  "type": "number"
                },
                "confidence": {
                  "type": "number"
                },
                "name": {
                  "type": "string"
                }
              }
            },
            "botRequest": {
              "definitions": {
                "topIntentSchema": {
                  "type": "object",
                  "properties": {
                    "dialogCounter": {
                      "minItems": 0,
                      "items": {
                        "type": "number"
                      },
                      "contains": {
                        "allOf": [
                          {
                            "type": "number"
                          }
                        ]
                      },
                      "type": "array"
                    },
                    "confidence": {
                      "minItems": 0,
                      "items": {
                        "type": "number"
                      },
                      "contains": {
                        "allOf": [
                          {
                            "type": "number"
                          }
                        ]
                      },
                      "type": "array"
                    },
                    "lastLocDiff": {
                      "type": "number"
                    },
                    "name": {
                      "type": "string"
                    },
                    "numOccurrence": {
                      "type": "number"
                    }
                  }
                },
                "relations": {
                  "type": "object"
                },
                "relation": {
                  "type": "object"
                },
                "messageSchema": {
                  "required": [
                    "message",
                    "type",
                    "raw"
                  ],
                  "type": "object",
                  "properties": {
                    "dialogCounter": {
                      "type": "number"
                    },
                    "message": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "entitesSubstituted": {
                      "type": "string"
                    },
                    "raw": {
                      "type": "object"
                    }
                  }
                },
                "context": {
                  "type": "object",
                  "properties": {
                    "conversationId": {
                      "type": "string"
                    },
                    "contextElements": {
                      "minItems": 0,
                      "items": {
                        "$ref": "#/definitions/contextElement"
                      },
                      "contains": {
                        "allOf": [
                          {
                            "$ref": "#/definitions/contextElement"
                          }
                        ]
                      },
                      "type": "array"
                    },
                    "topIntents": {
                      "minItems": 0,
                      "items": {
                        "$ref": "#/definitions/topIntentSchema"
                      },
                      "contains": {
                        "allOf": [
                          {
                            "$ref": "#/definitions/topIntentSchema"
                          }
                        ]
                      },
                      "type": "array"
                    },
                    "entities": {
                      "minItems": 0,
                      "items": {
                        "$ref": "#/definitions/entity"
                      },
                      "contains": {
                        "allOf": [
                          {
                            "$ref": "#/definitions/entity"
                          }
                        ]
                      },
                      "type": "array"
                    }
                  }
                },
                "contextElement": {
                  "required": [
                    "conversationId",
                    "intents",
                    "entities",
                    "nodesVisited",
                    "raw",
                    "dialogCounter"
                  ],
                  "type": "object",
                  "properties": {
                    "raw": {
                      "type": "object"
                    },
                    "intents": {
                      "minItems": 0,
                      "items": {
                        "$ref": "#/definitions/intent"
                      },
                      "contains": {
                        "allOf": [
                          {
                            "$ref": "#/definitions/intent"
                          }
                        ]
                      },
                      "type": "array"
                    },
                    "dialogCounter": {
                      "type": "number"
                    },
                    "nodesVisited": {
                      "minItems": 0,
                      "items": {
                        "$ref": "#/definitions/dialogNode"
                      },
                      "contains": {
                        "allOf": [
                          {
                            "$ref": "#/definitions/dialogNode"
                          }
                        ]
                      },
                      "type": "array"
                    },
                    "conversationId": {
                      "type": "string"
                    },
                    "entities": {
                      "minItems": 0,
                      "items": {
                        "$ref": "#/definitions/entity"
                      },
                      "contains": {
                        "allOf": [
                          {
                            "$ref": "#/definitions/entity"
                          }
                        ]
                      },
                      "type": "array"
                    }
                  }
                },
                "entites": {
                  "required": [
                    "name",
                    "confidence",
                    "dialogCounter",
                    "literalLength",
                    "type",
                    "literal",
                    "location"
                  ],
                  "type": "object",
                  "properties": {
                    "confidence": {
                      "type": "number"
                    },
                    "name": {
                      "type": "string"
                    },
                    "literalLength": {
                      "type": "number"
                    },
                    "dialogCounter": {
                      "type": "number"
                    },
                    "literal": {
                      "type": "string"
                    },
                    "location": {
                      "type": "array"
                    },
                    "type": {
                      "type": "string"
                    }
                  }
                },
                "intent": {
                  "required": [
                    "name",
                    "confidence",
                    "dialogCounter"
                  ],
                  "type": "object",
                  "properties": {
                    "dialogCounter": {
                      "type": "number"
                    },
                    "confidence": {
                      "type": "number"
                    },
                    "name": {
                      "type": "string"
                    }
                  }
                },
                "dialogNode": {
                  "required": [
                    "name",
                    "id",
                    "description"
                  ],
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "number"
                    },
                    "name": {
                      "type": "string"
                    },
                    "id": {
                      "type": "number"
                    }
                  }
                },
                "entity": {
                  "required": [
                    "name",
                    "confidence",
                    "dialogCounter",
                    "literalLength",
                    "type",
                    "literal",
                    "location"
                  ],
                  "type": "object",
                  "properties": {
                    "confidence": {
                      "type": "number"
                    },
                    "name": {
                      "type": "string"
                    },
                    "literalLength": {
                      "type": "number"
                    },
                    "dialogCounter": {
                      "type": "number"
                    },
                    "literal": {
                      "type": "string"
                    },
                    "location": {
                      "type": "array"
                    },
                    "type": {
                      "type": "string"
                    }
                  }
                }
              },
              "type": "object",
              "properties": {
                "info": {
                  "type": "object"
                },
                "context": {
                  "$ref": "#/definitions/context"
                },
                "templateBotTag": {
                  "type": "string"
                },
                "rawInput": {
                  "type": "object"
                },
                "message": {
                  "$ref": "#/definitions/messageSchema"
                },
                "computeRelations": {
                  "minItems": 0,
                  "items": {
                    "$ref": "#/definitions/relation"
                  },
                  "contains": {
                    "allOf": [
                      {
                        "$ref": "#/definitions/relation"
                      }
                    ]
                  },
                  "type": "array"
                }
              }
            },
            "entity": {
              "required": [
                "name",
                "confidence",
                "dialogCounter",
                "literalLength",
                "type",
                "literal",
                "location"
              ],
              "type": "object",
              "properties": {
                "confidence": {
                  "type": "number"
                },
                "name": {
                  "type": "string"
                },
                "literalLength": {
                  "type": "number"
                },
                "dialogCounter": {
                  "type": "number"
                },
                "literal": {
                  "type": "string"
                },
                "location": {
                  "type": "array"
                },
                "type": {
                  "type": "string"
                }
              }
            }
          },
          "inputMessage": {
            "required": [
              "message",
              "type",
              "raw"
            ],
            "type": "object",
            "properties": {
              "dialogCounter": {
                "type": "number"
              },
              "message": {
                "type": "string"
              },
              "type": {
                "type": "string"
              },
              "entitesSubstituted": {
                "type": "string"
              },
              "raw": {
                "type": "object"
              }
            }
          }
        }
      },
      "generalSchema": {
        "type": "object",
        "properties": {
          "tag": {
            "type": "string"
          }
        }
      },
      "AuthForm": {
        "type": "object",
        "properties": {
          "info": {
            "type": "object"
          },
          "componentType": {
            "patternProperties": {
              "progBinaryName": "AuthForm"
            },
            "type": "string"
          },
          "message": {
            "type": "string"
          },
          "tag": {
            "type": "string"
          },
          "name": {
            "type": "string"
          }
        }
      },
      "SimpleMessage": {
        "type": "object",
        "properties": {
          "componentType": {
            "patternProperties": {
              "progBinaryName": "SimpleMessage"
            },
            "type": "string"
          },
          "message": {
            "type": "string"
          },
          "tag": {
            "type": "string"
          },
          "data": {
            "type": "object"
          },
          "name": {
            "type": "string"
          }
        }
      },
      "Bubble": {
        "type": "object",
        "properties": {
          "uiReturnData": {
            "type": "object"
          },
          "componentType": {
            "patternProperties": {
              "progBinaryName": "Bubble"
            },
            "type": "string"
          },
          "message": {
            "type": "string"
          },
          "tag": {
            "type": "string"
          },
          "name": {
            "type": "string"
          }
        }
      },
      "CallBackForm": {
        "type": "object",
        "properties": {
          "info": {
            "type": "object"
          },
          "componentType": {
            "patternProperties": {
              "progBinaryName": "CallBackForm"
            },
            "type": "string"
          },
          "message": {
            "type": "string"
          },
          "tag": {
            "type": "string"
          },
          "name": {
            "type": "string"
          }
        }
      }
    },
    "type": "object",
    "properties": {
      "botResponse": {
        "$ref": "#/definitions/botResponse"
      },
      "uiComponents": {
        "minItems": 0,
        "items": {
          "anyOf": [
            {
              "$ref": "#/definitions/CallBackForm"
            },
            {
              "$ref": "#/definitions/AuthForm"
            },
            {
              "$ref": "#/definitions/Feedback"
            },
            {
              "$ref": "#/definitions/SimpleMessage"
            },
            {
              "$ref": "#/definitions/Bubble"
            },
            {
              "$ref": "#/definitions/UIComponentSchema"
            }
          ]
        },
        "contains": {
          "anyOf": [
            {
              "$ref": "#/definitions/CallBackForm"
            },
            {
              "$ref": "#/definitions/AuthForm"
            },
            {
              "$ref": "#/definitions/Feedback"
            },
            {
              "$ref": "#/definitions/SimpleMessage"
            },
            {
              "$ref": "#/definitions/Bubble"
            },
            {
              "$ref": "#/definitions/UIComponentSchema"
            }
          ]
        },
        "type": "array"
      }
    }
  },
  "Bubble": {
    "type": "object",
    "properties": {
      "uiReturnData": {
        "type": "object"
      },
      "componentType": {
        "patternProperties": {
          "progBinaryName": "Bubble"
        },
        "type": "string"
      },
      "message": {
        "type": "string"
      },
      "tag": {
        "type": "string"
      },
      "name": {
        "type": "string"
      }
    }
  },
  "dialogNode": {
    "required": [
      "name",
      "id",
      "description"
    ],
    "type": "object",
    "properties": {
      "description": {
        "type": "number"
      },
      "name": {
        "type": "string"
      },
      "id": {
        "type": "number"
      }
    }
  },
  "CallBackForm": {
    "type": "object",
    "properties": {
      "info": {
        "type": "object"
      },
      "componentType": {
        "patternProperties": {
          "progBinaryName": "CallBackForm"
        },
        "type": "string"
      },
      "message": {
        "type": "string"
      },
      "tag": {
        "type": "string"
      },
      "name": {
        "type": "string"
      }
    }
  }
}











