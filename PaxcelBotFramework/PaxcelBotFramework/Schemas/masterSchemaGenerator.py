import os
import importlib
import json


def getMasterSchema():

    full_path = os.path.realpath(__file__)
    dirName=os.path.dirname(full_path)
    filesNames=[(schema.split(".")[0],schema.split("_")[1].split(".")[0]) for schema in os.listdir(dirName) if schema.startswith("schema") and schema.endswith("py") ]

    #print filesNames
    masterSchema={}

    for (schemaFile,schemaName) in filesNames:
        masterSchema[schemaName] = importlib.import_module(schemaFile).schema


    return masterSchema

masterSchema =getMasterSchema()

print json.dumps(masterSchema,indent=2)

# copy the above output to masterSchema.py

#print schemas
#print json.dumps(masterSchema,indent=2)

#json.dump(open("mas"))