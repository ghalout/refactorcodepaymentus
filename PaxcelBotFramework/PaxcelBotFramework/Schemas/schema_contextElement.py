from schema_intents import schema as intentSchema
from schema_entities import schema as entitySchema
from schema_dialogNode import schema as dialogNodeSchema
from schema_relations import schema as relationSchema
import jsonschema
schema={
    "type": "object",
    "properties": {
        "conversationId": {"type":"string"},
        "intents":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/intent"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/intent"}

        },
        "entities":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/entity"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/entity"}

        },
        "nodesVisited":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/dialogNode"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/dialogNode"}

        },
        "raw":{
            "type":"object"

        },
        "dialogCounter":{"type":"number"}
    }
    ,
    "definitions":{
        "intent":{

  "type": "object",
  "properties": {
    "name": {"type":"string"},
    "confidence": {"type":"number"},
    "dialogCounter": {"type":"number"}
  },
  "required":["name","confidence","dialogCounter"]
},
        "entity":{
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "confidence": {
      "type": "number"
    },
    "dialogCounter": {
      "type": "number"
    },
    "location": {
      "type": "array"
    },
    "literal": {
      "type": "string"
    },
    "literalLength": {
      "type": "number"
    },
    "type": {
      "type": "string"
    }
  },
  "required":["name","confidence","dialogCounter","literalLength","type","literal","location"]
},
        "dialogNode":{

  "type": "object",
  "properties": {
    "name": {"type":"string"},
    "id": {"type":"number"},
    "description": {"type":"number"}
  },
    "required":["name","id","description"]
},
        "relations":{

  "type": "object"

}

    },
    "required":["conversationId","intents","entities","nodesVisited","raw","dialogCounter"]
}

"""
intent = {
    "name": "vaibhav",
    "confidence": 2.0,
    "dialogCounter": 2.0
}

elem={
    "conversationId":"sdf",
    "intents":[intent,{"asdf":34},intent]

}

jsonschema.validate(elem,schema=schema)
"""

"""

    def toDict(self):
        contextElement={}
        if isinstance(self.conversationId,tuple) or isinstance(self.conversationId,list):
            contextElement["conversationId"]=self.conversationId[0]
        else:
            contextElement["conversationId"]=self.conversationId

        contextElement["intents"]=[intent.toDict() for intent in self.intents]
        if self.entities:
            contextElement["entities"]=[entity.toDict() for entity in self.entities]
        else:
            contextElement["entities"]=[]

        contextElement["nodesVisited"]=[node.toDict() for node in self.nodesVisited]
        contextElement["relations"]=[relation.toDict() for relation in self.relations]
        contextElement["raw"]=self.raw#).replace("True","'True'"))
        if isinstance(self.dialogCounter,tuple) or isinstance(self.dialogCounter,list):
            contextElement["dialogCounter"] = self.dialogCounter[0]
        else:
            contextElement["dialogCounter"]=self.dialogCounter

        return contextElement


"""