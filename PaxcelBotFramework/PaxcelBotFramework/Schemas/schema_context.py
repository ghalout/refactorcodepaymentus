from schema_contextElement import schema as contextElementSchema
from schema_entities import schema as entitySchema
"""
    def __init__(self,conversationId="",contextElements=None,topIntents=None,entities=None):


        self.conversationId=conversationId

        if contextElements:
            self.contextElements=contextElements
        else:
            self.contextElements=[]

        if topIntents:
            self.topIntents=topIntents
        else:
            self.topIntents={}

        if entities:
            self.entities=entities
        else:
            self.entities = []


"entities": [
            {
                "confidence": 1,
                "name": "employee",
                "literalLength": 3,
                "dialogCounter": 1,
                "literal": "who",
                "location": [
                    1,
                    4
                ],
                "type": "knowAbout"
            }
"""
schema={
    "type": "object",
    "properties": {
        "conversationId": {"type":"string"},
        "contextElements":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/contextElement"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/contextElement"}

        },
        "entities":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/entity"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/entity"}
        },
        "topIntents":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/topIntentSchema"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/topIntentSchema"}

        },


},
    "definitions":{
        "contextElement": {
    "type": "object",
    "properties": {
        "conversationId": {"type":"string"},
        "intents":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/intent"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/intent"}

        },
        "entities":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/entity"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/entity"}

        },
        "nodesVisited":{
            "type":"array",
            "contains":{
                        "allOf":[{"$ref": "#/definitions/dialogNode"}]
            },
            "minItems":0,
            "items":{"$ref": "#/definitions/dialogNode"}

        },
        "raw":{
            "type":"object"

        },
        "dialogCounter":{"type":"number"}
    },

    "required":["conversationId","intents","entities","nodesVisited","raw","dialogCounter"]
},
        "intent":{

  "type": "object",
  "properties": {
    "name": {"type":"string"},
    "confidence": {"type":"number"},
    "dialogCounter": {"type":"number"}
  },
  "required":["name","confidence","dialogCounter"]
},
        "entity":{
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "confidence": {
      "type": "number"
    },
    "dialogCounter": {
      "type": "number"
    },
    "location": {
      "type": "array"
    },
    "literal": {
      "type": "string"
    },
    "literalLength": {
      "type": "number"
    },
    "type": {
      "type": "string"
    }
  },
  "required":["name","confidence","dialogCounter","literalLength","type","literal","location"]
},
        "dialogNode":{

  "type": "object",
  "properties": {
    "name": {"type":"string"},
    "id": {"type":"number"},
    "description": {"type":"number"}
  },
    "required":["name","id","description"]
},
        "relations":{

  "type": "object"

},
        "entites":entitySchema,
        "topIntentSchema":{
            "type":"object",
            "properties":{
                "name":{"type":"string"},
                "dialogCounter":{
                    "type":"array",
                    "contains":{
                        "allOf":[{"type":"number"}]
                    },
                    "minItems":0,
                    "items":{"type":"number"}
                    },
                "confidence":{
                    "type":"array",
                    "contains":{
                        "allOf":[{"type":"number"}]
                    },
                    "minItems":0,
                    "items":{"type":"number"}
                    },
                "lastLocDiff": {"type":"number"},
                "numOccurrence": {"type":"number"}
            }

    }}
}




"""
"topIntents": {
            "getProjectEmployees": {
                "dialogCounters": [
                    1
                ],
                "confidence": [
                    0.7438078403472901
                ],
                "lastLocDiff": 0,
                "numOccurrence": 1
            }
        }

"""