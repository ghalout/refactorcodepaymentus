schema = {
    "type": "object",

    "properties": {

        "info": {"type": "object"},

        "message": {"type": "string"},

        "tag": {"type": "string"},

        "componentType": {"type": "string",
                          "patternProperties": {
                              "progBinaryName": "AuthForm"
                          }

                          },  # this property is common to all types of UiComponents rest may change

        "name": {"type": "string"}

    }

}