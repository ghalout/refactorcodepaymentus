schema = {
    "type": "object",

    "properties": {

        "uiReturnData": {"type": "object"}, # return karne pe kya karna hai bhai

        "message": {"type": "string"}, # jo display karana hai

        "tag": {"type": "string"}, # tag for message formation layer

        "componentType":{"type":"string",
          "patternProperties": {
                "progBinaryName": "Bubble" # To check if user writes component type properly Bubble
            }

            },                   # this property is common to all types of UiComponents rest may change

        "name":{"type": "string"} # component name

    }

}